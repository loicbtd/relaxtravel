var VueCategorieProduit = (function () {

    emplacement = document.getElementById('emplacement-donnees');
    titre = document.getElementById('titre');

    return function () {

        this.afficher = function () {

            var xmlhttp = new XMLHttpRequest();
           
            xmlhttp.onreadystatechange = function() {
        
                if (this.readyState == 4 && this.status == 200) {
        
                    // alert(this.responseText);
                    jsonData = JSON.parse(this.responseText);
                    // console.log(jsonData.data);
                    
                    emplacement.innerHTML = "";
                    titre.innerHTML = "Liste des catégories de produits";

                    table = document.createElement("table");
                    table.setAttribute("class", "pure-table pure-table-horizontal");
                    table.setAttribute("id", "table");

                    thead = document.createElement("thead");
                    tr = document.createElement("tr");

                    id = document.createElement("th");
                    id.innerHTML = "#Id Categorie";
                    tr.appendChild(id);
                    
                    nom = document.createElement("th");
                    nom.innerHTML = "Dénommination";
                    tr.appendChild(nom);

                    suppr = document.createElement("th");
                    suppr.innerHTML = "Modification";
                    tr.appendChild(suppr);

                    thead.appendChild(tr);
                    table.appendChild(thead);

                    tbody = document.createElement("tbody");
                    tbody.setAttribute("id","table-body");

                    listeCategoriesProduits = [];

                    for (let i = 0; i < jsonData.data.length; i++) {

                        listeCategoriesProduits = [];
                        listeCategoriesProduits.push(new CategorieProduit(jsonData.data[i].id_categorie_article, jsonData.data[i].etiquette));
                                
                        tr = document.createElement("tr");
                        td = document.createElement("td");
                        td.style.fontWeight = "bold";
                        td.innerHTML = '<a onclick="consulterCatProduitModal('+ jsonData.data[i].id_categorie_article + ',' + i + ')" class="acces-button acces-button-a">' + jsonData.data[i].id_categorie_article + '</a> '
                        tr.appendChild(td);

                        td = document.createElement("td");
                        td.setAttribute("id","id-cat-prod-" + i);
                        td.innerHTML = jsonData.data[i].etiquette;
                        tr.appendChild(td);

                        td = document.createElement("td");
                        td.innerHTML = '<input type="button" onclick="editCatProduitModal('+ jsonData.data[i].id_categorie_article + ',' + i + ')" value="&#xf044" class="fa fa-input acces-button"><input type="button" onclick="supprCatProduitModal('+ jsonData.data[i].id_categorie_article + ',' + i + ')" value="&#xf1f8" class="fa fa-input acces-button">';
                        tr.appendChild(td);

                        tbody.appendChild(tr);
                        
                    }

                    table.appendChild(tbody);
                    
                    emplacement.appendChild(table);

                    ajouter = document.createElement("button");
                    ajouter.setAttribute("class","pure-button pure-button-primary espacement-2");
                    ajouter.setAttribute("onclick","ajouterCatProduitModal()");
                    
                    ajouter.innerHTML = "Ajouter une catégorie de produits";

                    emplacement.appendChild(ajouter);
        
                }
        
            };
            xmlhttp.open("GET", "http://relaxtravel.adrianpaul-carrieres.fr/categories-article", true);
            xmlhttp.send();

        }

    }

})();