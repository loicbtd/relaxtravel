var VueAccueil = (function () {
    var pageAccueil = document.getElementById("page_accueil").innerHTML;
    var header = document.getElementById("header").innerHTML;

    return function () {

        this.afficher = function () {
            elementBody = document.getElementsByTagName("body")[0];
            elementBody.innerHTML = header + pageAccueil;
            this.startTime();
            this.getEmplacements();
        };

        this.startTime = function () {
            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById("heure").innerHTML = h;
            document.getElementById("minute").innerHTML = m;
            document.getElementById("seconde").innerHTML = s;


            var t = setTimeout(startTime, 500);
        };

        this.getEmplacements = function () {
            var xmlhttp=new XMLHttpRequest();
            xmlhttp.onreadystatechange=function(){
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    console.log(xmlhttp.responseText);
                    var data = JSON.parse(xmlhttp.responseText);
                    console.log(data);
                    var emplacements = data.data;
                    console.log(emplacements);
                    var select = "";
                    console.log(emplacements);
                    for (let i = 0; i < emplacements.length; i++) {
                        select+="<option value='"+emplacements[i].id_emplacement+"'>"+emplacements[i].etiquette+"</option>"
                    }
                    document.getElementById("choix_emplacement").innerHTML = select;
                }
            };
            xmlhttp.open("GET","http://relaxtravel.adrianpaul-carrieres.fr/emplacements",true);
            xmlhttp.send();
        }
    }

})();