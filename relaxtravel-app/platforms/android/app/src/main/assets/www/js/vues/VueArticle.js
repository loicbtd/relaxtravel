var VueArticle = (function () {
    var pageArticle = document.getElementById("page_article").innerHTML;
    var header = document.getElementById("header").innerHTML;

    return function (idArticle) {

        this.afficher = function () {
            var etiquette ="Article";
            var code = 123456123;
            var categorie = "Produits Fragiles";
            var quantite = 0;
            var today = new Date();
            var date = today.toJSON().slice(0, 10);
            var nDate = date.slice(8, 10) + '/'
                + date.slice(5, 7) + '/'
                + date.slice(0, 4);
            var caracs = "Lourd, Froid";
            elementBody = document.getElementsByTagName("body")[0];
            elementBody.innerHTML = header + pageArticle;
            JsBarcode("#code_barre", code, {
                width: 2,
                height: 50,
                displayValue: true
            });



            document.getElementById("etiquette_objet").innerHTML = etiquette;
            document.getElementById("categorie_objet").innerHTML = categorie;
            document.getElementById("carac_objet").innerHTML = caracs;
            document.getElementById("date_ajout").innerHTML = "date : "+ nDate;

            document.getElementById("btn_dl_objet").onclick = this.telechargerEtiquette;
            document.getElementById("btn_modifier_objet").onclick = function(){
                window.location.href = "#modif-article/"+idArticle
            };

            document.getElementById("btn_supprimer_objet").onclick = this.supprimerObjet;
            this.afficherDonnees();
            this.afficherQuantite();
            // document.getElementById("btn_modifier_objet").onclick = rediriger vers page modification;

        };

        this.afficherQuantite = function () {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function(){
                if (xhttp.readyState==4 && xhttp.status==200) {
                    console.log(xhttp.responseText);
                    var quantite = JSON.parse(xhttp.responseText).data[0].sum;
                    console.log(JSON.parse(xhttp.responseText));
                    document.getElementById("quantite_objet").innerHTML = quantite;
                }
            };
            xhttp.open("GET", "http://relaxtravel.adrianpaul-carrieres.fr/stock?id_article="+idArticle+"&id_emplacement="+idEmplacement, true);
            xhttp.send();
        };


        this.supprimerObjet = async function () {
            console.log("suppression");
            await supprimerArticle(idArticle);
            window.location.href = "#liste-articles";
            console.log("id-article supprimé ",idArticle);
            document.getElementById("element_"+idArticle).remove();
        };

        this.telechargerEtiquette = function () {
            const element = document.getElementById("etiquette");
            var opt = {
                margin:       0.5,
                filename:     'myfile.pdf',
                image:        { type: 'jpeg', quality: 0.98 },
                html2canvas:  { scale: 2 },
                jsPDF:        { unit: 'in', format: 'A5', orientation: 'portrait' }
            };
            // Choose the element and save the PDF for our user.
            html2pdf()
                .set(opt)
                .from(element)
                .save("etiquette");
        };

        this.afficherDonnees = function () {
            var xmlhttp=new XMLHttpRequest();
            xmlhttp.onreadystatechange=function(){
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    console.log(xmlhttp.responseText);
                    var article = JSON.parse(xmlhttp.responseText).data[0];
                    console.log(article);
                    document.getElementById("etiquette_objet").innerHTML = article.etiquette;
                    document.getElementById("carac_objet").innerHTML = article.caracteristiques;
                    xhttp2 = new XMLHttpRequest();
                    xhttp2.onreadystatechange = function(){
                        if (xhttp2.readyState==4 && xhttp2.status==200) {
                            console.log(xhttp2.responseText);
                            var reponse = JSON.parse(xhttp2.responseText).data[0];
                            document.getElementById("categorie_objet").innerHTML = reponse.etiquette
                        }
                    };
                    xhttp2.open("GET", "http://relaxtravel.adrianpaul-carrieres.fr/categories-article/"+article.id_categorie_article);
                    xhttp2.send();
                        JsBarcode("#code_barre", article.code, {
                            width: 2,
                            height: 50,
                            displayValue: true
                        });


                }
            };
            xmlhttp.open("GET","http://relaxtravel.adrianpaul-carrieres.fr/articles/"+idArticle,true);
            xmlhttp.send();
        }

    }
})();