var VueAjoutArticle = (function () {
    var pageAjout = document.getElementById("page_ajout").innerHTML;
    var header = document.getElementById("header").innerHTML;

    return function () {
        this.code = "";
        this.afficher = function () {
            elementBody = document.getElementsByTagName("body")[0];
            elementBody.innerHTML = header + pageAjout;
            JsBarcode("#code_barre", "123456123", {
                width: 2,
                height: 50,
                displayValue: true
            });
            document.getElementById("btn_dl_ajout").onclick = this.telechargerEtiquette;
            document.getElementById("btn_ajouter").onclick = this.ajouter;
            var inputs = document.getElementsByTagName("input");
            for (let i = 0; i < inputs.length ; i++) {
                inputs[i].oninput = this.majEtiquette;
            }
            var selects = document.getElementsByTagName("select");
            for (let i = 0; i < selects.length ; i++) {
                selects[i].oninput = this.majEtiquette;
            }
            this.getCategories();
            this.code = "";
            do {
                this.code ="";
                for (let i = 0; i < 13; i++) {
                    this.code+= Math.floor(Math.random() * Math.floor(10));
                }
                console.log(this.isExiste(this.code));
            }while (this.isExiste(this.code));
            if (this.code != ""){
                document.getElementById("code_ajout").value = this.code;
                format: "ean13",
                    JsBarcode("#code_barre", this.code, {
                        width: 2,
                        height: 50,
                        displayValue: true
                    });
            }
        };

        this.majEtiquette = function () {
            document.getElementById("etiqu_etiquette").innerHTML = document.getElementById("champ_etiquette").value;
            document.getElementById("etiqu_categorie").innerHTML = document.getElementById("option_"+document.getElementById("champ_categorie").value).innerHTML;
            document.getElementById("etiqu_quantite").innerHTML = document.getElementById("champ_quantite").value;
            document.getElementById("etiqu_carac").innerHTML = document.getElementById("champ_carac").value;
            var today = new Date();
            var date = today.toJSON().slice(0, 10);
            var nDate = date.slice(8, 10) + '/'
                + date.slice(5, 7) + '/'
                + date.slice(0, 4);
            document.getElementById("etiqu_date").innerHTML = nDate;

        };

        this.telechargerEtiquette = function () {
            const element = document.getElementById("etiquette");
            var opt = {
                margin:       0.5,
                filename:     'myfile.pdf',
                image:        { type: 'jpeg', quality: 0.98 },
                html2canvas:  { scale: 2 },
                jsPDF:        { unit: 'in', format: 'A5', orientation: 'portrait' }
            };
            // Choose the element and save the PDF for our user.
            html2pdf()
                .set(opt)
                .from(element)
                .save("etiquette");
        };

        this.ajouter = function () {
            var erreurs = verificationForm();
            if (erreurs){
                console.log("pas ajout");
            }else{
                var code,idCategorie, etiquette, caracteristiques;
                code = document.getElementById("code_ajout").value;
                idCategorie = document.getElementById("champ_categorie").value;
                etiquette =  document.getElementById("champ_etiquette").value;
                caracteristiques = document.getElementById("champ_carac").value;
                console.log(code, idCategorie, etiquette, caracteristiques);
                console.log("ajout");
                var reussi = false;
                var xmlhttp=new XMLHttpRequest();
                xmlhttp.onreadystatechange=function(){
                    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                        console.log(xmlhttp.responseText);
                        var reponse = JSON.parse(xmlhttp.responseText);
                        if (reponse.status == "success"){
                            var idArticle = reponse.id;
                            console.log(idArticle);
                            ajouterHistorique(idArticle,1);
                            window.location.href = "#liste-articles";
                        }
                    }
                };
                xmlhttp.open("POST","http://relaxtravel.adrianpaul-carrieres.fr/articles",true);
                xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlhttp.send("code="+code+"&id_categorie_article="+idCategorie+"&etiquette="+etiquette+"&caracteristiques="+caracteristiques);
                console.log(reussi)

            }
        };



        this.getCategories = function () {
            var xmlhttp=new XMLHttpRequest();
            xmlhttp.onreadystatechange=function(){
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    console.log(xmlhttp.responseText);
                    var data = JSON.parse(xmlhttp.responseText);
                    console.log(data);
                    var categories = data.data;
                    console.log(categories);
                    var select = "";
                    console.log(categories);
                    for (let i = 0; i < categories.length; i++) {
                        select+="<option id='option_"+categories[i].id_categorie_article+"' value='"+categories[i].id_categorie_article+"'>"+categories[i].etiquette+"</option>"
                    }
                    document.getElementById("champ_categorie").innerHTML = select;
                    document.getElementById("etiqu_categorie").innerHTML = document.getElementById("option_1").innerHTML;

                }
            };
            xmlhttp.open("GET","http://relaxtravel.adrianpaul-carrieres.fr/categories-article",true);
            xmlhttp.send();
        };

        function verificationForm () {
            var erreurs = false;
            var msgerreurs = document.getElementsByClassName("msgerr");
            for (let i = 0; i < msgerreurs.length ; i++) {
                msgerreurs[i].style.display = "none";
            }
            if (document.getElementById("champ_etiquette").value == ""){
                document.getElementById("message_etiquette").style.display = "block";
                erreurs = true
            }
            if (document.getElementById("champ_quantite").value == "" || document.getElementById("champ_quantite").value == 0 ){
                document.getElementById("message_quantite_nulle").style.display = "block";
                erreurs = true
            }

            if (isNaN(document.getElementById("champ_quantite").value)){
                document.getElementById("message_quantite_nan").style.display = "block";
                erreurs = true
            }
            if (document.getElementById("champ_carac").value == ""){
                document.getElementById("message_caracs").style.display = "block";
                erreurs = true
            }

            return erreurs;

        }

        this.isExiste = function (code) {
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (xhttp.readyState==4 && xhttp.status==200) {
                    if (JSON.parse(xhttp.responseText).status != "error") {
                        console.log("existe pas")
                    }
                }
            };
            xhttp.open("GET", "http://relaxtravel.adrianpaul-carrieres.fr/articles-code/"+code, true);
            xhttp.send();
            return false
        }
    }
})();