var ajouterUtilisateur = function() {

    var identifiant = document.getElementById('identifiant').value;
    var code = document.getElementById('code').value;
    var id_type_utilisateur = parseInt(document.getElementById('categories-utilisateurs').selectedOptions[0].value);
    var mot_de_passe = document.getElementById('m-d-p').innerHTML;

    var identifiantValide = (identifiant.length > 1);
    var codeValide = (is_int(code) && (code.length == 13));

    if (identifiantValide && codeValide) {
        
        document.getElementById('erreur-identifiant').style.display = "none";
        document.getElementById('identifiant').style.borderColor = "inherit";

        document.getElementById('erreur-code').style.display = "none";
        document.getElementById('code').style.borderColor = "inherit";

        var utilisateur = new Utilisateur(code,identifiant,id_type_utilisateur,mot_de_passe);
        var utilisateurData = JSON.stringify(utilisateur);
        console.log(utilisateurData);
        
        closeModal();

        var xmlhttp = new XMLHttpRequest();  

        xmlhttp.onreadystatechange = function() {

            if (this.readyState == 4 && this.status == 200) {
                // alert(this.responseText);
                afficherModalSucces("Ajout de l'utilisateur confirmé");

            }

        };
        xmlhttp.open("POST", "http://relaxtravel.adrianpaul-carrieres.fr/utilisateurs", true);
        xmlhttp.setRequestHeader("Content-type","application/json");
        xmlhttp.send(utilisateurData);

    } else {
        
        if (identifiantValide) {
            document.getElementById('erreur-identifiant').style.display = "none";
            document.getElementById('identifiant').style.borderColor = "inherit";
        } else {
            document.getElementById('erreur-identifiant').style.display = "block";
            document.getElementById('identifiant').style.borderColor = "red";
        }

        if (codeValide) {
            document.getElementById('erreur-code').style.display = "none";
            document.getElementById('code').style.borderColor = "inherit";
        } else {
            document.getElementById('erreur-code').style.display = "block";
            document.getElementById('code').style.borderColor = "red";
            
        }      
        
    }

}

var editUtilisateur = function(id) {

    var identifiant = document.getElementById('identifiant').value;
    var code = document.getElementById('code').value;
    var id_type_utilisateur = parseInt(document.getElementById('categories-utilisateurs').selectedOptions[0].value);
    var mot_de_passe = document.getElementById('m-d-p').innerHTML;

    var identifiantValide = (identifiant.length > 1);
    var codeValide = (is_int(code) && (code.length == 13));

    if (identifiantValide && codeValide) {
        
        document.getElementById('erreur-identifiant').style.display = "none";
        document.getElementById('identifiant').style.borderColor = "inherit";

        document.getElementById('erreur-code').style.display = "none";
        document.getElementById('code').style.borderColor = "inherit";

        var utilisateur = new Utilisateur(code,identifiant,id_type_utilisateur,mot_de_passe);
        var utilisateurData = JSON.stringify(utilisateur);
        console.log(utilisateurData);
        
        closeModal();

        var xmlhttp = new XMLHttpRequest();  

        xmlhttp.onreadystatechange = function() {

            if (this.readyState == 4 && this.status == 200) {
                // alert(this.responseText);
                afficherModalSucces("Modification de l'utilisateur #" + id + " confirmée");

            }

        };
        xmlhttp.open("PUT", "http://relaxtravel.adrianpaul-carrieres.fr/utilisateurs/" + id, true);
        xmlhttp.setRequestHeader("Content-type","application/json");
        xmlhttp.send(utilisateurData);

    } else {
        
        if (identifiantValide) {
            document.getElementById('erreur-identifiant').style.display = "none";
            document.getElementById('identifiant').style.borderColor = "inherit";
        } else {
            document.getElementById('erreur-identifiant').style.display = "block";
            document.getElementById('identifiant').style.borderColor = "red";
        }

        if (codeValide) {
            document.getElementById('erreur-code').style.display = "none";
            document.getElementById('code').style.borderColor = "inherit";
        } else {
            document.getElementById('erreur-code').style.display = "block";
            document.getElementById('code').style.borderColor = "red";
            
        }      
        
    }


    
}

var supprUtilisateur = function(id) {

    closeModal();

    var xmlhttp = new XMLHttpRequest();  

    xmlhttp.onreadystatechange = function() {

        if (this.readyState == 4 && this.status == 200) {
            // alert(this.responseText);
            afficherModalSucces("Suppression de l'utilisateur #" + id + " confirmée");

        }

    };
    xmlhttp.open("DELETE", "http://relaxtravel.adrianpaul-carrieres.fr/utilisateurs/" + id, true);
    xmlhttp.setRequestHeader("Content-type","application/json");
    xmlhttp.send();
    
}