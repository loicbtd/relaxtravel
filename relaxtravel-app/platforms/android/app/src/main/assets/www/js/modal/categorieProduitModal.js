var ajouterCatProduitModal = function() {

    document.getElementById('titre-modal').innerHTML = "Ajouter une catégorie de produits";

    var fieldset = document.getElementById('fieldset-modal');
    fieldset.innerHTML = "";

    div = document.createElement("div");
    div.setAttribute("class","pure-control-group");
    
    label = document.createElement("label");
    label.setAttribute("for","etiquette");
    label.innerHTML = "Nom de la catégorie";
    div.appendChild(label);

    input = document.createElement("input");
    input.setAttribute("id","etiquette");
    input.setAttribute("type","text");
    input.setAttribute("placeholder","nom catégorie");
    div.appendChild(input);

    fieldset.appendChild(div);

    label = document.createElement("label");
    label.setAttribute("for","etiquette");
    label.setAttribute("id","erreur-etiquette");
    label.innerHTML = "Le nom doit avoir au moins 2 caractères";
    label.style.color = "red";
    label.style.display = "none";
    label.style.marginLeft = "9rem";
    fieldset.appendChild(label);


    div = document.createElement("div");
    div.setAttribute("class","pure-control-group center espacement-3-2");

    input = document.createElement("input");
    input.setAttribute("type","button");
    input.setAttribute("id","ajouterCatProd");
    input.setAttribute("class","pure-button pure-button-primary");
    input.setAttribute("onclick","ajouterCatProduit()");
    input.setAttribute("value","Ajouter");
    div.appendChild(input);

    fieldset.appendChild(div);

    openModal();

}

//TODO consulterCatProduitModal(id,ligne)
var consulterCatProduitModal = function(id, ligne) {

    document.getElementById('titre-modal').innerHTML = "Produit présent dans la catégorie #" + id;

    var fieldset = document.getElementById('fieldset-modal');
    fieldset.innerHTML = "";

    alert("BLOP " + id + "," + ligne);
    
     

    openModal();
    
}

var editCatProduitModal = function(id, ligne) {

    document.getElementById('titre-modal').innerHTML = "Modifier la catégorie de produits #" + id;

    var fieldset = document.getElementById('fieldset-modal');
    fieldset.innerHTML = "";

    div = document.createElement("div");
    div.setAttribute("class","pure-control-group");
    
    label = document.createElement("label");
    label.setAttribute("for","etiquette");
    label.innerHTML = "Nom de la catégorie";
    div.appendChild(label);

    input = document.createElement("input");
    input.setAttribute("id","etiquette");
    input.setAttribute("type","text");
    input.setAttribute("placeholder","nom catégorie");
    valueInput = document.getElementById("id-cat-prod-" + ligne).innerHTML;
    input.setAttribute("value",valueInput);
    div.appendChild(input);

    fieldset.appendChild(div);

    label = document.createElement("label");
    label.setAttribute("for","etiquette");
    label.setAttribute("id","erreur-etiquette");
    label.innerHTML = "Le nom doit avoir au moins 2 caractères";
    label.style.color = "red";
    label.style.display = "none";
    label.style.marginLeft = "9rem";
    fieldset.appendChild(label);

    div = document.createElement("div");
    div.setAttribute("class","pure-control-group center espacement-3-2");

    input = document.createElement("input");
    input.setAttribute("type","button");
    input.setAttribute("id","editCatProd");
    input.setAttribute("class","pure-button pure-button-primary");
    input.setAttribute("onclick","editCatProduit(" + id + ")");
    input.setAttribute("value","Modifier");
    div.appendChild(input);

    fieldset.appendChild(div);

    openModal();
    
}

var supprCatProduitModal = function(id, ligne) {

    document.getElementById('titre-modal').innerHTML = "Voulez-vous supprimer la catégorie de produits #" + id + " ?";

    var fieldset = document.getElementById('fieldset-modal');
    fieldset.innerHTML = "";

    div = document.createElement("div");
    div.setAttribute("class","pure-control-group center");
    
    label = document.createElement("label");
    label.setAttribute("for","etiquette");
    label.style.width = "auto";
    label.innerHTML = "Nom de la catégorie";
    div.appendChild(label);

    label = document.createElement("label");
    label.style.width = "auto";
    label.style.fontWeight = "bold";
    valueInput = document.getElementById("id-cat-prod-" + ligne).innerHTML;
    label.innerHTML = valueInput;
    div.appendChild(label);

    fieldset.appendChild(div);

    div = document.createElement("div");
    div.setAttribute("class","pure-control-group center espacement-3-2");

    input = document.createElement("input");
    input.setAttribute("type","button");
    input.setAttribute("id","supprCatProd");
    input.setAttribute("class","pure-button pure-button-primary");
    input.setAttribute("onclick","supprCatProduit(" + id + ")");
    input.setAttribute("value","Supprimer");
    div.appendChild(input);

    fieldset.appendChild(div);

    openModal();
    
}