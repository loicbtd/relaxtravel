var ajouterUtilisateurModal = function() {

    document.getElementById('titre-modal').innerHTML = "Ajouter un utilisateur";

    var fieldset = document.getElementById('fieldset-modal');
    fieldset.innerHTML = "";

    div = document.createElement("div");
    div.setAttribute("class","pure-control-group");
    
    label = document.createElement("label");
    label.setAttribute("for","identifiant");
    label.innerHTML = "Identifiant de l'utilisateur";
    div.appendChild(label);

    input = document.createElement("input");
    input.setAttribute("id","identifiant");
    input.setAttribute("type","text");
    input.setAttribute("placeholder","identifiant utilisateur");
    div.appendChild(input);

    fieldset.appendChild(div);

    label = document.createElement("label");
    label.setAttribute("for","identifiant");
    label.setAttribute("id","erreur-identifiant");
    label.innerHTML = "L'identifiant doit avoir au moins 2 caractères";
    label.style.color = "red";
    label.style.display = "none";
    label.style.marginLeft = "9rem";
    fieldset.appendChild(label);

    //
    div = document.createElement("div");
    div.setAttribute("class","pure-control-group");
    
    label = document.createElement("label");
    label.setAttribute("for","code");
    label.innerHTML = "Code de l'utilisateur";
    div.appendChild(label);

    input = document.createElement("input");
    input.setAttribute("id","code");
    input.setAttribute("type","text");
    input.setAttribute("placeholder","code utilisateur");
    div.appendChild(input);

    fieldset.appendChild(div);

    label = document.createElement("label");
    label.setAttribute("for","etiquette");
    label.setAttribute("id","erreur-code");
    label.innerHTML = "Le code doit être un nombre entier de 13 digits";
    label.style.color = "red";
    label.style.display = "none";
    label.style.marginLeft = "9rem";
    fieldset.appendChild(label);

    //
    div = document.createElement("div");
    div.setAttribute("class","pure-control-group");

    label = document.createElement("label");
    label.setAttribute("for","categories-utilisateurs");
    label.innerHTML = "Catégorie de l'utilisateur";
    div.appendChild(label);

    select = document.createElement("select");
    select.setAttribute("name","categories-utilisateurs");
    select.setAttribute("id","categories-utilisateurs");
    select.setAttribute("onchange","changerMotDePasseUtilisateurModal()");

    for (let i = 0; i < listeCategoriesUtilisateurs.length; i++) {

        option = document.createElement("option");
        option.setAttribute("value",listeCategoriesUtilisateurs[i].id_type_utilisateur);
        option.innerHTML = "" + listeCategoriesUtilisateurs[i].id_type_utilisateur + " - " + listeCategoriesUtilisateurs[i].etiquette;
        
        select.appendChild(option);

    }

    div.appendChild(select);
    fieldset.appendChild(div);

    //
    div = document.createElement("div");
    div.setAttribute("class","pure-control-group");
    
    label = document.createElement("label");
    label.setAttribute("for","m-d-p");
    label.innerHTML = "Mot de passe utilisateur";
    div.appendChild(label);

    label = document.createElement("label");
    label.setAttribute("id","m-d-p");
    label.innerHTML = listeCategoriesUtilisateurs[0].etiquette;
    div.appendChild(label);

    fieldset.appendChild(div);


    div = document.createElement("div");
    div.setAttribute("class","pure-control-group center espacement-3-2");

    input = document.createElement("input");
    input.setAttribute("type","button");
    input.setAttribute("id","ajouterUtili");
    input.setAttribute("class","pure-button pure-button-primary");
    input.setAttribute("onclick","ajouterUtilisateur()");
    input.setAttribute("value","Ajouter");
    div.appendChild(input);

    fieldset.appendChild(div);

    openModal();

}

var consulterUtilisateurModal = function(id, ligne) {

    document.getElementById('titre-modal').innerHTML = "Utilisateur #" + id;

    var fieldset = document.getElementById('fieldset-modal');
    fieldset.innerHTML = "";

    container = document.createElement("div");
    container.setAttribute("class","center");

    div = document.createElement("div");
    div.setAttribute("class","pure-control-group");
    
    label = document.createElement("label");
    label.setAttribute("for","identifiant");
    label.style.width = "auto";
    label.innerHTML = "Identifiant de l'utilisateur";
    div.appendChild(label);

    label = document.createElement("label");
    label.setAttribute("id","identifiant");
    label.style.width = "auto";
    label.style.fontWeight = "bold";
    valueInput = document.getElementById('identifiant-' + ligne).innerHTML;
    label.innerHTML = valueInput;
    div.appendChild(label);

    container.appendChild(div);

    //
    div = document.createElement("div");
    div.setAttribute("class","pure-control-group");
    
    label = document.createElement("label");
    label.setAttribute("for","code");
    label.style.width = "auto";
    label.innerHTML = "Code de l'utilisateur";
    div.appendChild(label);

    label = document.createElement("label");
    label.setAttribute("id","code");
    label.style.width = "auto";
    label.style.fontWeight = "bold";
    valueInput = document.getElementById('code-' + ligne).innerHTML;
    label.innerHTML = valueInput;
    div.appendChild(label);

    container.appendChild(div);

    //
    div = document.createElement("div");
    div.setAttribute("class","pure-control-group");

    label = document.createElement("label");
    label.setAttribute("for","categories-utilisateurs");
    label.innerHTML = "Catégorie de l'utilisateur";
    div.appendChild(label);

    label = document.createElement("label");
    label.setAttribute("name","categories-utilisateurs");
    label.setAttribute("id","categories-utilisateurs");
    label.style.width = "auto";
    label.style.fontWeight = "bold";

    for (let i = 0; i < listeCategoriesUtilisateurs.length; i++) {

        if (listeCategoriesUtilisateurs[i].id_type_utilisateur == parseInt(document.getElementById("type-utilisateur-" + ligne).innerHTML)) {
            label.innerHTML = listeCategoriesUtilisateurs[i].etiquette;
            break;
        }

    }

    div.appendChild(label);
    container.appendChild(div);

    fieldset.appendChild(container);

    div = document.createElement("div");
    div.setAttribute("class","pure-control-group center espacement-3-2");

    input = document.createElement("input");
    input.setAttribute("type","button");
    input.setAttribute("id","supprUtili");
    input.setAttribute("class","pure-button pure-button-primary");
    input.setAttribute("onclick","closeModal()");
    input.setAttribute("value","Fermer");
    div.appendChild(input);

    fieldset.appendChild(div);

    openModal();
    
}

var editUtilisateurModal = function(id, ligne) {

    document.getElementById('titre-modal').innerHTML = "Modifier l'utilisateur #" + id;

    var fieldset = document.getElementById('fieldset-modal');
    fieldset.innerHTML = "";

    div = document.createElement("div");
    div.setAttribute("class","pure-control-group");
    
    label = document.createElement("label");
    label.setAttribute("for","identifiant");
    label.innerHTML = "Identifiant de l'utilisateur";
    div.appendChild(label);

    input = document.createElement("input");
    input.setAttribute("id","identifiant");
    input.setAttribute("type","text");
    input.setAttribute("placeholder","identifiant utilisateur");
    valueInput = document.getElementById('identifiant-' + ligne).innerHTML;
    input.value = valueInput;
    div.appendChild(input);

    fieldset.appendChild(div);

    label = document.createElement("label");
    label.setAttribute("for","identifiant");
    label.setAttribute("id","erreur-identifiant");
    label.innerHTML = "L'identifiant doit avoir au moins 2 caractères";
    label.style.color = "red";
    label.style.display = "none";
    label.style.marginLeft = "9rem";
    fieldset.appendChild(label);

    //
    div = document.createElement("div");
    div.setAttribute("class","pure-control-group");
    
    label = document.createElement("label");
    label.setAttribute("for","code");
    label.innerHTML = "Code de l'utilisateur";
    div.appendChild(label);

    input = document.createElement("input");
    input.setAttribute("id","code");
    input.setAttribute("type","text");
    input.setAttribute("placeholder","code utilisateur");
    valueInput = document.getElementById('code-' + ligne).innerHTML;
    input.value = valueInput;
    div.appendChild(input);

    fieldset.appendChild(div);

    label = document.createElement("label");
    label.setAttribute("for","etiquette");
    label.setAttribute("id","erreur-code");
    label.innerHTML = "Le code doit être un nombre entier de 13 digits";
    label.style.color = "red";
    label.style.display = "none";
    label.style.marginLeft = "9rem";
    fieldset.appendChild(label);

    //
    div = document.createElement("div");
    div.setAttribute("class","pure-control-group");

    label = document.createElement("label");
    label.setAttribute("for","categories-utilisateurs");
    label.innerHTML = "Catégorie de l'utilisateur";
    div.appendChild(label);

    select = document.createElement("select");
    select.setAttribute("name","categories-utilisateurs");
    select.setAttribute("id","categories-utilisateurs");
    select.setAttribute("onchange","changerMotDePasseUtilisateurModal()");

    valueStartSelect = -1;
    for (let i = 0; i < listeCategoriesUtilisateurs.length; i++) {

        option = document.createElement("option");
        option.setAttribute("value",listeCategoriesUtilisateurs[i].id_type_utilisateur);
        option.innerHTML = "" + listeCategoriesUtilisateurs[i].id_type_utilisateur + " - " + listeCategoriesUtilisateurs[i].etiquette;
        
        select.appendChild(option);

        if (listeCategoriesUtilisateurs[i].id_type_utilisateur == parseInt(document.getElementById("type-utilisateur-" + ligne).innerHTML)) {
            select.selectedIndex = i;
            valueStartSelect = i;
        }

    }

    div.appendChild(select);
    fieldset.appendChild(div);

    //
    div = document.createElement("div");
    div.setAttribute("class","pure-control-group");
    
    label = document.createElement("label");
    label.setAttribute("for","m-d-p");
    label.innerHTML = "Mot de passe utilisateur";
    div.appendChild(label);

    label = document.createElement("label");
    label.setAttribute("id","m-d-p");
    label.innerHTML = listeCategoriesUtilisateurs[valueStartSelect].etiquette;
    div.appendChild(label);

    fieldset.appendChild(div);


    div = document.createElement("div");
    div.setAttribute("class","pure-control-group center espacement-3-2");

    input = document.createElement("input");
    input.setAttribute("type","button");
    input.setAttribute("id","editUtili");
    input.setAttribute("class","pure-button pure-button-primary");
    input.setAttribute("onclick","editUtilisateur( " + id + " )");
    input.setAttribute("value","Modifier");
    div.appendChild(input);

    fieldset.appendChild(div);

    openModal();

}

var supprUtilisateurModal = function(id, ligne) {

    document.getElementById('titre-modal').innerHTML = "Voulez-vous supprimer l'utilisateur #" + id + " ?";

    var fieldset = document.getElementById('fieldset-modal');
    fieldset.innerHTML = "";

    container = document.createElement("div");
    container.setAttribute("class","center");

    div = document.createElement("div");
    div.setAttribute("class","pure-control-group");
    
    label = document.createElement("label");
    label.setAttribute("for","identifiant");
    label.style.width = "auto";
    label.innerHTML = "Identifiant de l'utilisateur";
    div.appendChild(label);

    label = document.createElement("label");
    label.setAttribute("id","identifiant");
    label.style.width = "auto";
    label.style.fontWeight = "bold";
    valueInput = document.getElementById('identifiant-' + ligne).innerHTML;
    label.innerHTML = valueInput;
    div.appendChild(label);

    container.appendChild(div);

    //
    div = document.createElement("div");
    div.setAttribute("class","pure-control-group");
    
    label = document.createElement("label");
    label.setAttribute("for","code");
    label.style.width = "auto";
    label.innerHTML = "Code de l'utilisateur";
    div.appendChild(label);

    label = document.createElement("label");
    label.setAttribute("id","code");
    label.style.width = "auto";
    label.style.fontWeight = "bold";
    valueInput = document.getElementById('code-' + ligne).innerHTML;
    label.innerHTML = valueInput;
    div.appendChild(label);

    container.appendChild(div);

    //
    div = document.createElement("div");
    div.setAttribute("class","pure-control-group");

    label = document.createElement("label");
    label.setAttribute("for","categories-utilisateurs");
    label.innerHTML = "Catégorie de l'utilisateur";
    div.appendChild(label);

    label = document.createElement("label");
    label.setAttribute("name","categories-utilisateurs");
    label.setAttribute("id","categories-utilisateurs");
    label.style.width = "auto";
    label.style.fontWeight = "bold";

    for (let i = 0; i < listeCategoriesUtilisateurs.length; i++) {

        if (listeCategoriesUtilisateurs[i].id_type_utilisateur == parseInt(document.getElementById("type-utilisateur-" + ligne).innerHTML)) {
            label.innerHTML = listeCategoriesUtilisateurs[i].etiquette;
            break;
        }

    }

    div.appendChild(label);
    container.appendChild(div);

    fieldset.appendChild(container);

    div = document.createElement("div");
    div.setAttribute("class","pure-control-group center espacement-3-2");

    input = document.createElement("input");
    input.setAttribute("type","button");
    input.setAttribute("id","supprUtili");
    input.setAttribute("class","pure-button pure-button-primary");
    input.setAttribute("onclick","supprUtilisateur( " + id + " )");
    input.setAttribute("value","Supprimer");
    div.appendChild(input);

    fieldset.appendChild(div);

    openModal();
    
}