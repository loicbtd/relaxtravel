var idUtilisateur;
var idEmplacement =1;
var connecte = false;

function supprimerArticle(idArticle) {
    console.log("suppression");
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState==4 && xhttp.status==200) {
            console.log(xhttp.responseText);


        }
    };
    xhttp.open("DELETE", "http://relaxtravel.adrianpaul-carrieres.fr/articles/"+idArticle);
    xhttp.send();
}

function supprimerListe(idArticle) {
    console.log("suppression");
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState==4 && xhttp.status==200) {
            console.log(xhttp.responseText);
        }
    };
    xhttp.open("DELETE", "http://relaxtravel.adrianpaul-carrieres.fr/articles/"+idArticle);
    xhttp.send();
    document.getElementById("element_"+idArticle).remove();

}


function ajouterHistorique (idArticle, transaction) {
    var  historique = '{' +
        '"id_article":'+ idArticle+', ' +
        '"id_utilisateur":'+idUtilisateur+',' +
        ' "id_emplacement":'+idEmplacement+',' +
        '"quantite":'+document.getElementById("champ_quantite").value+',' +
        '"id_type_transaction":'+transaction+'}';
    console.log(historique);
    console.log(historique);
    var xhttp2=new XMLHttpRequest();
    xhttp2.onreadystatechange=function(){
        if (xhttp2.readyState==4 && xhttp2.status==200) {
            console.log(xhttp2.responseText);
        }
    };
    xhttp2.open("POST","http://relaxtravel.adrianpaul-carrieres.fr/historiques-stock",true);
    xhttp2.setRequestHeader("Content-type", "application/json");
    xhttp2.send(historique);
};
(function () {

    var initialiser = function () {
        window.addEventListener("hashchange", naviguer);
        naviguer();
    };


    var choixEmplacement = function () {
        idEmplacement = document.getElementById("choix_emplacement").value
        console.log(idEmplacement)
    };


    var getIdentifiant = function () {
        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                console.log(xhttp.responseText);
                var utilisateur = JSON.parse(xhttp.responseText).data[0];
                document.getElementById("login_header").innerHTML = utilisateur.identifiant;
            }
        };
        xhttp.open("GET", "http://relaxtravel.adrianpaul-carrieres.fr/utilisateurs/"+idUtilisateur, true);
        xhttp.send();
    }
    var naviguer = function(){
        var hash = window.location.hash;

        if (!hash /*|| !connecte*/){
            var pageConnexion = new VueConnexion();
            pageConnexion.afficher();
        } else if(hash.match(/^#accueil/)){
            var pageAccueil = new VueAccueil();
            pageAccueil.afficher();
            document.getElementById("choix_emplacement").oninput = choixEmplacement;
            getIdentifiant();
        } else if(hash.match(/^#article\/([0-9]+)/)){
            var navigation = hash.match(/^#article\/([0-9]+)/);
            var idArticle = navigation[1];
            var pageArticle = new VueArticle(idArticle);
            pageArticle.afficher();
            getIdentifiant();
        }else if (hash.match(/^#liste-articles/)){
            var navigation = hash.match(/^#liste-articles/);
            var pageListe = new VueListeArticle();
            pageListe.afficher();
            getIdentifiant();
        }else if (hash.match(/^#scanner/)){
            var navigation = hash.match(/^#scanner/);
            var pageScanner = new VueScanner();
            pageScanner.afficher();
            getIdentifiant();
        }else if(hash.match(/^#modif-article\/([0-9]+)/)){
            var navigation = hash.match(/^#modif-article\/([0-9]+)/);
            var idArticle = navigation[1];
            var pageModif = new VueModificationArticle(idArticle);
            pageModif.afficher();
            getIdentifiant();
        }else if(hash.match(/^#ajouter-article/)){
            var navigation = hash.match(/^#ajouter-article/);
            var pageAjout = new VueAjoutArticle();
            pageAjout.afficher();
            getIdentifiant();
        }
    };

    initialiser()

})();