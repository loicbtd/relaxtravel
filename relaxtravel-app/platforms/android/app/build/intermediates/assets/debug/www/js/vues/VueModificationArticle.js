var VueModificationArticle = (function () {
    var pageModification = document.getElementById("page_modif").innerHTML;
    var header = document.getElementById("header").innerHTML;

    return function (idArticle) {

        this.afficher = function () {
            elementBody = document.getElementsByTagName("body")[0];
            elementBody.innerHTML = header + pageModification;
            JsBarcode("#code_barre", "123456123", {
                width: 2,
                height: 50,
                displayValue: true
            });
            document.getElementById("btn_dl_modification").onclick = this.telechargerEtiquette;
            document.getElementById("btn_modifier").onclick = this.modifier;
            var inputs = document.getElementsByTagName("input");

            for (let i = 0; i < inputs.length ; i++) {
                inputs[i].oninput = this.majEtiquette;
            }
            var selects = document.getElementsByTagName("select");
            for (let i = 0; i < selects.length ; i++) {
                selects[i].oninput = this.majEtiquette;
            }

            this.getCategories();
            this.afficherDonnees();
            var today = new Date();
            var date = today.toJSON().slice(0, 10);
            var nDate = date.slice(8, 10) + '/'
                + date.slice(5, 7) + '/'
                + date.slice(0, 4);
            document.getElementById("etiqu_date").innerHTML = nDate;
            var code = document.getElementById("champ_code").value;
            if (code != ""){
                format: "ean13",
                    JsBarcode("#code_barre", code, {
                        width: 2,
                        height: 50,
                        displayValue: true
                    });
            }

        };

        this.majEtiquette = function () {
            document.getElementById("etiqu_etiquette").innerHTML = document.getElementById("champ_etiquette").value;
            document.getElementById("etiqu_categorie").innerHTML = document.getElementById("option_"+document.getElementById("champ_categorie").value).innerHTML;
            document.getElementById("etiqu_quantite").innerHTML = document.getElementById("champ_quantite").value;
            document.getElementById("etiqu_carac").innerHTML = document.getElementById("champ_carac").value;
            var today = new Date();
            var date = today.toJSON().slice(0, 10);
            var nDate = date.slice(8, 10) + '/'
                + date.slice(5, 7) + '/'
                + date.slice(0, 4);
            document.getElementById("etiqu_date").innerHTML = nDate;
            var code = document.getElementById("champ_code").value;

        };

        this.telechargerEtiquette = function () {
            const element = document.getElementById("etiquette");
            var opt = {
                margin:       0.5,
                filename:     'myfile.pdf',
                image:        { type: 'jpeg', quality: 0.98 },
                html2canvas:  { scale: 2 },
                jsPDF:        { unit: 'in', format: 'A5', orientation: 'portrait' }
            };
            // Choose the element and save the PDF for our user.
            html2pdf()
                .set(opt)
                .from(element)
                .save("etiquette");
        };

        this.getCategories = function () {
            var xmlhttp=new XMLHttpRequest();
            xmlhttp.onreadystatechange=function(){
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    console.log(xmlhttp.responseText);
                    var data = JSON.parse(xmlhttp.responseText);
                    console.log(data);
                    var categories = data.data;
                    console.log(categories);
                    var select = "";
                    console.log(categories);
                    for (let i = 0; i < categories.length; i++) {
                        select+="<option id='option_"+categories[i].id_categorie_article+"' value='"+categories[i].id_categorie_article+"'>"+categories[i].etiquette+"</option>"
                    }
                    document.getElementById("champ_categorie").innerHTML = select;
                }
            };
            xmlhttp.open("GET","http://relaxtravel.adrianpaul-carrieres.fr/categories-article",true);
            xmlhttp.send();
        };

        this.modifier = function () {
            var erreurs = verificationForm();
            if (erreurs){
                console.log("pas modification");
            }else {
                var code,idCategorie, etiquette, caracteristiques;
                idCategorie = document.getElementById("champ_categorie").value;
                etiquette =  document.getElementById("champ_etiquette").value;
                caracteristiques = document.getElementById("champ_carac").value;
                code = document.getElementById("champ_code").value;
                console.log(code, idCategorie, etiquette, caracteristiques);
                console.log("ajout");
                var xmlhttp=new XMLHttpRequest();
                xmlhttp.onreadystatechange=function(){
                    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                        console.log(xmlhttp.responseText);
                        var reponse = JSON.parse(xmlhttp.responseText);
                        if (reponse.message == "Modification réussie"){
                            console.log("modifreussie");
                            ajouterHistorique(idArticle,2);
                            window.location.href = "#liste-articles";
                        }
                    }
                };
                xmlhttp.open("PUT","http://relaxtravel.adrianpaul-carrieres.fr/articles/"+idArticle,true);
                xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xmlhttp.send("code="+code+"&id_categorie_article="+idCategorie+"&etiquette="+etiquette+"&caracteristiques="+caracteristiques);
            }
        };

        this.afficherDonnees = function () {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function(){
                if (xhttp.readyState==4 && xhttp.status==200) {
                    var reponse = JSON.parse(xhttp.responseText).data[0].sum;
                    console.log(JSON.parse(xhttp.responseText));
                    document.getElementById("champ_quantite").value = parseInt(reponse);
                    document.getElementById("etiqu_quantite").innerHTML = reponse;
                }
            };
            xhttp.open("GET", "http://relaxtravel.adrianpaul-carrieres.fr/stock?id_article="+idArticle+"&id_emplacement="+idEmplacement, true);
            xhttp.send();
            var xmlhttp=new XMLHttpRequest();
            xmlhttp.onreadystatechange=function(){
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    console.log(xmlhttp.responseText);
                    var article = JSON.parse(xmlhttp.responseText).data[0];
                    console.log(article);
                    document.getElementById("champ_etiquette").value = article.etiquette;
                    document.getElementById("champ_code").value = article.code;
                    document.getElementById("champ_carac").value = article.caracteristiques;
                    document.getElementById("champ_categorie").selectedIndex = parseInt(article.id_categorie_article)-1;
                    console.log( document.getElementById("champ_categorie").selected);
                    console.log(parseInt(article.id_categorie_article));
                    document.getElementById("etiqu_etiquette").innerHTML = document.getElementById("champ_etiquette").value;
                    document.getElementById("etiqu_categorie").innerHTML = document.getElementById("option_"+document.getElementById("champ_categorie").value).innerHTML;
                    document.getElementById("etiqu_carac").innerHTML = document.getElementById("champ_carac").value;
                        format: "ean13",
                            JsBarcode("#code_barre", article.code, {
                                width: 2,
                                height: 50,
                                displayValue: true
                            });


                }
            };
            xmlhttp.open("GET","http://relaxtravel.adrianpaul-carrieres.fr/articles/"+idArticle,true);
            xmlhttp.send();

        };


        function verificationForm () {
            var erreurs = false;
            var msgerreurs = document.getElementsByClassName("msgerr");
            for (let i = 0; i < msgerreurs.length ; i++) {
                msgerreurs[i].style.display = "none";
            }
            if (document.getElementById("champ_etiquette").value == ""){
                document.getElementById("message_etiquette").style.display = "block";
                erreurs = true
            }
            if (document.getElementById("champ_quantite").value == "" || document.getElementById("champ_quantite").value == 0 ){
                document.getElementById("message_quantite_nulle").style.display = "block";
                erreurs = true
            }

            if (isNaN(document.getElementById("champ_quantite").value)){
                document.getElementById("message_quantite_nan").style.display = "block";
                erreurs = true
            }
            if (document.getElementById("champ_carac").value == ""){
                document.getElementById("message_caracs").style.display = "block";
                erreurs = true
            }

            return erreurs;

        }


    }
})();