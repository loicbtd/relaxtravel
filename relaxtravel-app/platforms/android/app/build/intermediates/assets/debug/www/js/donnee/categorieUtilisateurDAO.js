var ajouterCatUtilisateur = function() {

    var etiquette = document.getElementById('etiquette').value;

    if (etiquette.length < 2) {
        
        document.getElementById('erreur-etiquette').style.display = "block";
        document.getElementById('etiquette').style.borderColor = "red";

    } else {

        document.getElementById('erreur-etiquette').style.display = "none";
        document.getElementById('etiquette').style.borderColor = "inherit";

        var categorie = new CategorieUtilisateurBDD(etiquette);
        var categorieData = JSON.stringify(categorie);
        // console.log(categorieData);
        
        closeModal();

        var xmlhttp = new XMLHttpRequest();  

        xmlhttp.onreadystatechange = function() {

            if (this.readyState == 4 && this.status == 200) {
                afficherModalSucces("Ajout de la catégorie d'utilisateurs confirmé");
            }

        };
        xmlhttp.open("POST", "http://relaxtravel.adrianpaul-carrieres.fr/types-utilisateur", true);
        xmlhttp.setRequestHeader("Content-type","application/json");
        xmlhttp.send(categorieData);

    }

}

//TODO consulterCatUtilisateur(id)
var consulterCatUtilisateur = function(id) {
    
}

var editCatUtilisateur = function(id) {
    
    var etiquette = document.getElementById('etiquette').value;

    if (etiquette.length < 2) {
        
        document.getElementById('erreur-etiquette').style.display = "block";
        document.getElementById('etiquette').style.borderColor = "red";

    } else {

        document.getElementById('erreur-etiquette').style.display = "none";
        document.getElementById('etiquette').style.borderColor = "inherit";

        var categorie = new CategorieUtilisateurBDD(etiquette);
        var categorieData = JSON.stringify(categorie);
        // console.log(categorieData);
        
        closeModal();

        var xmlhttp = new XMLHttpRequest();  

        xmlhttp.onreadystatechange = function() {

            if (this.readyState == 4 && this.status == 200) {
                afficherModalSucces("Modification de la catégorie d'utilisateurs #" + id + " confirmée");
            }

        };
        xmlhttp.open("PUT", "http://relaxtravel.adrianpaul-carrieres.fr/types-utilisateur/" + id, true);
        xmlhttp.setRequestHeader("Content-type","application/json");
        xmlhttp.send(categorieData);

    }

}

var supprCatUtilisateur = function(id) {

    closeModal();

    var xmlhttp = new XMLHttpRequest();  

    xmlhttp.onreadystatechange = function() {

        if (this.readyState == 4 && this.status == 200) {
            afficherModalSucces("Suppression de la catégorie d'utilisateurs #" + id + " confirmée");
        }

    };
    xmlhttp.open("DELETE", "http://relaxtravel.adrianpaul-carrieres.fr/types-utilisateur/" + id, true);
    xmlhttp.setRequestHeader("Content-type","application/json");
    xmlhttp.send();
    
}