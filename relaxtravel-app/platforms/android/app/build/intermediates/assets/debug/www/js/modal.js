// Get the modal
var modal = document.getElementById('modal');

// Get the <span> element that closes the modal
var span = document.getElementById('close');

var closeModal = function() {
    modal.style.display = "none";
}

var openModal = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    closeModal();
}
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        closeModal();
    }
}

var fermerModal = function() {

    var hash = window.location.hash;

    if(!hash) {

        accueil.afficher();

    } else if(hash.match(/^#Entrepots/)) {

        entrepots.afficher();
        
    } else if(hash.match(/^#Produits/)) {

        produits.afficher();
        
    } else if(hash.match(/^#Categories-Produits/)) {
        
        categorieProduit.afficher();
        
    } else if(hash.match(/^#Categories-Utilisateurs/)){

        categorieUtilisateur.afficher();

    } else if(hash.match(/^#Utilisateurs/)){

        utilisateurs.afficher();

    }

    closeModal();
    
}

var afficherModalSucces = function(titre) {
    
    var fieldset = document.getElementById('fieldset-modal');
    fieldset.innerHTML = ""
    document.getElementById('titre-modal').innerHTML = titre;
    
    div = document.createElement("div");
    div.setAttribute("class","pure-control-group center espacement-3-2");

    input = document.createElement("input");
    input.setAttribute("type","button");
    input.setAttribute("id","fermerMod");
    input.setAttribute("class","pure-button pure-button-primary");
    input.setAttribute("onclick","fermerModal()");
    input.setAttribute("value","Fermer");
    div.appendChild(input);

    fieldset.appendChild(div);

    openModal();

}

var changerMotDePasseUtilisateurModal = function() {

    for (let i = 0; i < listeCategoriesUtilisateurs.length; i++) {

        if (listeCategoriesUtilisateurs[i].id_type_utilisateur == parseInt(document.getElementById('categories-utilisateurs').selectedOptions[0].value)) {
            document.getElementById('m-d-p').innerHTML = listeCategoriesUtilisateurs[i].etiquette;
            break;
        }
        
    }

}

