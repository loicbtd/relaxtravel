var VueScanner = (function () {
    var pageScanner = document.getElementById("page_scanner").innerHTML;
    var header = document.getElementById("header").innerHTML;

    return function () {

        this.afficher = function () {
            elementBody = document.getElementsByTagName("body")[0];
            elementBody.innerHTML = header + pageScanner;
            this.scanner();
        }



        this.scanner = function () {
            let selectedDeviceId;
            const codeReader = new ZXing.BrowserBarcodeReader()
            console.log('ZXing code reader initialized')
            codeReader.getVideoInputDevices()
                .then((videoInputDevices) => {
                    const sourceSelect = document.getElementById('sourceSelect')
                    selectedDeviceId = videoInputDevices[0].deviceId
                    if (videoInputDevices.length > 1) {
                        videoInputDevices.forEach((element) => {
                            const sourceOption = document.createElement('option')
                            sourceOption.text = element.label
                            sourceOption.value = element.deviceId
                            sourceSelect.appendChild(sourceOption)
                        })

                        sourceSelect.onchange = () => {
                            selectedDeviceId = sourceSelect.value;
                        }

                        const sourceSelectPanel = document.getElementById('sourceSelectPanel')
                        sourceSelectPanel.style.display = 'block'
                    }

                    // document.getElementById('startButton').addEventListener('click', () => {
                    codeReader.decodeOnceFromVideoDevice(selectedDeviceId, 'video').then((result) => {
                        console.log(result)
                        document.getElementById('result').textContent = result.text;
                        var xmlhttp=new XMLHttpRequest();
                        xmlhttp.onreadystatechange=function(){
                            if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                                console.log(xmlhttp.responseText);
                                var reponse = JSON.parse(xmlhttp.responseText).message;
                                console.log("#article/"+reponse[0].id_article);
                                window.location.href = "#article/"+reponse[0].id_article;

                            }
                        };
                        xmlhttp.open("GET","http://relaxtravel.adrianpaul-carrieres.fr/articles-code/"+result.text,true);
                        xmlhttp.send();
                        document.getElementById('result').innerHTML = "<span class='text-red-600'>aucun objet trouvé, veuillez réessayer</span>"
                    }).catch((err) => {
                        console.error(err)
                        document.getElementById('result').innerHTML = "<span class='text-red-600'>aucun objet trouvé, veuillez réessayer</span>"
                    })
                    console.log(`Started continous decode from camera with id ${selectedDeviceId}`)
                    // })

                    document.getElementById('resetButton').addEventListener('click', () => {
                        document.getElementById('result').textContent = '';
                        codeReader.reset();
                        console.log('Reset.')
                        codeReader.decodeOnceFromVideoDevice(selectedDeviceId, 'video').then((result) => {
                            console.log(result)
                            document.getElementById('result').textContent = result.text

                        }).catch((err) => {
                            console.error(err)
                            document.getElementById('result').textContent = err
                        });
                        console.log(`Started continous decode from camera with id ${selectedDeviceId}`)
                    })

                })
                .catch((err) => {
                    console.error(err)
                })
        }
    }
})();