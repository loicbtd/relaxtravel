var ajouterProduit = function() {

    var etiquette = document.getElementById('etiquette').value;
    var code = document.getElementById('code').value;
    var caracteristiques = document.getElementById('caract').value;
    var id_categorie_article = parseInt(document.getElementById('cat-prod').selectedOptions[0].value);

    var etiquetteValide = (etiquette.length > 1);
    var codeValide = (is_int(code) && (code.length == 13));
    var caracteristiquesValide = (caracteristiques.length > 0);


    if (etiquetteValide && codeValide && caracteristiquesValide) {

        document.getElementById('erreur-etiquette').style.display = "none";
        document.getElementById('etiquette').style.borderColor = "inherit";

        document.getElementById('erreur-code').style.display = "none";
        document.getElementById('code').style.borderColor = "inherit";

        document.getElementById('erreur-cara').style.display = "none";
        document.getElementById('caract').style.borderColor = "inherit";


        var produit = new Produit(etiquette, parseInt(code), caracteristiques, id_categorie_article);
        var produitData = JSON.stringify(produit);
        // console.log(produitData);

        closeModal();

        var xmlhttp = new XMLHttpRequest();

        xmlhttp.onreadystatechange = function() {

            if (this.readyState == 4 && this.status == 200) {
                // alert(this.responseText);
                afficherModalSucces("Ajout du produit confirmé");

            }

        };
        xmlhttp.open("POST", "http://relaxtravel.adrianpaul-carrieres.fr/articles", true);
        xmlhttp.setRequestHeader("Content-type", "application/json");
        xmlhttp.send(produitData);

    } else {

        if (etiquetteValide) {
            document.getElementById('erreur-etiquette').style.display = "none";
            document.getElementById('etiquette').style.borderColor = "inherit";
        } else {
            document.getElementById('erreur-etiquette').style.display = "block";
            document.getElementById('etiquette').style.borderColor = "red";
        }

        if (codeValide) {
            document.getElementById('erreur-code').style.display = "none";
            document.getElementById('code').style.borderColor = "inherit";
        } else {
            document.getElementById('erreur-code').style.display = "block";
            document.getElementById('code').style.borderColor = "red";

        }

        if (caracteristiquesValide) {
            document.getElementById('erreur-cara').style.display = "none";
            document.getElementById('caract').style.borderColor = "inherit";
        } else {
            document.getElementById('erreur-cara').style.display = "block";
            document.getElementById('caract').style.borderColor = "red";

        }

    }


}

var editProduit = function(id) {

    console.log("produit DAO id", id);

    var etiquette = document.getElementById('etiquette').value;
    var code = document.getElementById('code').value;
    var caracteristiques = document.getElementById('caracteristiques').value;
    var id_categorie_article = parseInt(document.getElementById('cat-prod').selectedOptions[0].value);

    var etiquetteValide = (etiquette.length > 1);
    var codeValide = (is_int(code) && (code.length == 13));
    var caracteristiquesValide = (caracteristiques.length > 0);


    if (etiquetteValide && codeValide && caracteristiquesValide) {

        document.getElementById('erreur-etiquette').style.display = "none";
        document.getElementById('etiquette').style.borderColor = "inherit";

        document.getElementById('erreur-code').style.display = "none";
        document.getElementById('code').style.borderColor = "inherit";

        document.getElementById('erreur-caracteristiques').style.display = "none";
        document.getElementById('caracteristiques').style.borderColor = "inherit";


        var produit = new Produit(etiquette, code, caracteristiques, id_categorie_article);
        var produitData = JSON.stringify(produit);
        console.log(produitData);

        closeModal();

        var xmlhttp = new XMLHttpRequest();

        xmlhttp.onreadystatechange = function() {
            console.log(xmlhttp.responseText);

            if (this.readyState == 4 && this.status == 200) {
                // alert(this.responseText);
                afficherModalSucces("Modification du produit confirmée");

            }

        };
        xmlhttp.open("PUT", "http://relaxtravel.adrianpaul-carrieres.fr/articles/" + id, true);
        xmlhttp.setRequestHeader("Content-type", "application/json");
        xmlhttp.send(produitData);

    } else {

        if (etiquetteValide) {
            document.getElementById('erreur-etiquette').style.display = "none";
            document.getElementById('etiquette').style.borderColor = "inherit";
        } else {
            document.getElementById('erreur-etiquette').style.display = "block";
            document.getElementById('etiquette').style.borderColor = "red";
        }

        if (codeValide) {
            document.getElementById('erreur-code').style.display = "none";
            document.getElementById('code').style.borderColor = "inherit";
        } else {
            document.getElementById('erreur-code').style.display = "block";
            document.getElementById('code').style.borderColor = "red";

        }

        if (caracteristiquesValide) {
            document.getElementById('erreur-caracteristiques').style.display = "none";
            document.getElementById('caracteristiques').style.borderColor = "inherit";
        } else {
            document.getElementById('erreur-caracteristiques').style.display = "block";
            document.getElementById('caracteristiques').style.borderColor = "red";

        }

    }


}

var supprProduit = function(id) {

    closeModal();

    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() {

        if (this.readyState == 4 && this.status == 200) {
            // alert(this.responseText);
            afficherModalSucces("Suppression du produit confirmée");

        }

    };
    xmlhttp.open("DELETE", "http://relaxtravel.adrianpaul-carrieres.fr/articles/" + id, true);
    xmlhttp.setRequestHeader("Content-type", "application/json");
    xmlhttp.send();


}