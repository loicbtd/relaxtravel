var ajouterEntrepot = function() {
   
    var etiquette = document.getElementById('etiquette').value;

    if (etiquette.length < 2) {
        
        document.getElementById('erreur-etiquette').style.display = "block";
        document.getElementById('etiquette').style.borderColor = "red";

    } else {

        document.getElementById('erreur-etiquette').style.display = "none";
        document.getElementById('etiquette').style.borderColor = "inherit";

        var entrepot = new Entrepot(etiquette);
        var etiquetteData = JSON.stringify(entrepot);
        // console.log(etiquetteData);
        
        closeModal();

        var xmlhttp = new XMLHttpRequest();  

        xmlhttp.onreadystatechange = function() {            

            if (this.readyState == 4 && this.status == 200) {
                afficherModalSucces("Ajout de l'entrepôt confirmé");
            }

        };
        xmlhttp.open("POST", "http://relaxtravel.adrianpaul-carrieres.fr/emplacements", true);
        xmlhttp.setRequestHeader("Content-type","application/json");
        xmlhttp.send(etiquetteData);
        
    }

}

//TODO consulterEntrpot(id)
var consulterEntrepot = function(id) {
    
}

var editEntrepot = function(id) {

    var etiquette = document.getElementById('etiquette').value;

    if (etiquette.length < 2) {
        
        document.getElementById('erreur-etiquette').style.display = "block";
        document.getElementById('etiquette').style.borderColor = "red";

    } else {

        document.getElementById('erreur-etiquette').style.display = "none";
        document.getElementById('etiquette').style.borderColor = "inherit";

        var entrepot = new Entrepot(etiquette);
        var etiquetteData = JSON.stringify(entrepot);
        console.log(etiquetteData);
        
        closeModal();

        var xmlhttp = new XMLHttpRequest();  

        xmlhttp.onreadystatechange = function() {

            if (this.readyState == 4 && this.status == 200) {

                // alert(this.responseText);

                afficherModalSucces("Modification de l'entrepôt #" + id + " confirmée");

            }

        };
        xmlhttp.open("PUT", "http://relaxtravel.adrianpaul-carrieres.fr/emplacements/" + id, true);
        xmlhttp.setRequestHeader("Content-type","application/json");
        xmlhttp.send(etiquetteData);
        
    }
    
}

var supprEntrepot = function(id) {

    closeModal();

    var xmlhttp = new XMLHttpRequest();  

    xmlhttp.onreadystatechange = function() {

        if (this.readyState == 4 && this.status == 200) {
            afficherModalSucces("Suppression de l'entrepôt #" + id + " confirmée");
        }

    };
    xmlhttp.open("DELETE", "http://relaxtravel.adrianpaul-carrieres.fr/emplacements/" + id, true);
    xmlhttp.setRequestHeader("Content-type","application/json");
    xmlhttp.send();

}