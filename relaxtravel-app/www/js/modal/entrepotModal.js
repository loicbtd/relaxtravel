var ajouterEntrepotModal = function() {
    
    document.getElementById('titre-modal').innerHTML = "Ajouter un entrepôt";

    var fieldset = document.getElementById('fieldset-modal');
    fieldset.innerHTML = "";

    div = document.createElement("div");
    div.setAttribute("class","pure-control-group");
    
    label = document.createElement("label");
    label.setAttribute("for","etiquette");
    label.innerHTML = "Nom de l'entrepôt";
    div.appendChild(label);

    input = document.createElement("input");
    input.setAttribute("id","etiquette");
    input.setAttribute("type","text");
    input.setAttribute("placeholder","nom entrepôt");
    div.appendChild(input);

    fieldset.appendChild(div);

    label = document.createElement("label");
    label.setAttribute("for","etiquette");
    label.setAttribute("id","erreur-etiquette");
    label.innerHTML = "Le nom doit avoir au moins 2 caractères";
    label.style.color = "red";
    label.style.display = "none";
    label.style.marginLeft = "9rem";
    fieldset.appendChild(label);


    div = document.createElement("div");
    div.setAttribute("class","pure-control-group center espacement-3-2");

    input = document.createElement("input");
    input.setAttribute("type","button");
    input.setAttribute("id","ajouterEntr");
    input.setAttribute("class","pure-button pure-button-primary");
    input.setAttribute("onclick","ajouterEntrepot()");
    input.setAttribute("value","Ajouter");
    div.appendChild(input);

    fieldset.appendChild(div);

    openModal();

}

//TODO consulterEntrepotModal(id, ligne)
var consulterEntrepotModal = function(id, ligne) {

    document.getElementById('titre-modal').innerHTML = "Contenu de l'entrepôt #" + id;

    var fieldset = document.getElementById('fieldset-modal');
    fieldset.innerHTML = "";

    alert("BLOP " + id + "," + ligne);
    
     // <div class="pure-control-group">
    //     <label for="name">Nom</label>
    //     <input id="name" type="text" placeholder="Nom">
    // </div>

    // <div class="pure-control-group center" id="validation">
    //     <input type="button" value="Enregistrer" class="pure-button pure-button-primary" id="enregistrer">
    // </div>

    openModal();

}

var editEntrepotModal = function(id, ligne) {
    
    document.getElementById('titre-modal').innerHTML = "Modifier l'entrepôt #" + id;

    var fieldset = document.getElementById('fieldset-modal');
    fieldset.innerHTML = "";

    div = document.createElement("div");
    div.setAttribute("class","pure-control-group");
    
    label = document.createElement("label");
    label.setAttribute("for","etiquette");
    label.innerHTML = "Nom de l'entrepôt";
    div.appendChild(label);

    input = document.createElement("input");
    input.setAttribute("id","etiquette");
    input.setAttribute("type","text");
    input.setAttribute("placeholder","nom entrepôt");
    valueInput = document.getElementById("id-" + ligne).innerHTML;
    input.setAttribute("value",valueInput);
    div.appendChild(input);

    fieldset.appendChild(div);

    label = document.createElement("label");
    label.setAttribute("for","etiquette");
    label.setAttribute("id","erreur-etiquette");
    label.innerHTML = "Le nom doit avoir au moins 2 caractères";
    label.style.color = "red";
    label.style.display = "none";
    label.style.marginLeft = "9rem";
    fieldset.appendChild(label);

    div = document.createElement("div");
    div.setAttribute("class","pure-control-group center espacement-3-2");

    input = document.createElement("input");
    input.setAttribute("type","button");
    input.setAttribute("id","editEntr");
    input.setAttribute("class","pure-button pure-button-primary");
    input.setAttribute("onclick","editEntrepot(" + id + ")");
    input.setAttribute("value","Modifier");
    div.appendChild(input);

    fieldset.appendChild(div);

    openModal();

}

var supprEntrepotModal = function(id, ligne) {

    document.getElementById('titre-modal').innerHTML = "Voulez-vous supprimer l'entrepôt #" + id + " ?";

    var fieldset = document.getElementById('fieldset-modal');
    fieldset.innerHTML = "";

    div = document.createElement("div");
    div.setAttribute("class","pure-control-group center");
    
    label = document.createElement("label");
    label.setAttribute("for","etiquette");
    label.style.width = "auto";
    label.innerHTML = "Nom de l'entrepôt";
    div.appendChild(label);

    label = document.createElement("label");
    label.style.width = "auto";
    label.style.fontWeight = "bold";
    valueInput = document.getElementById("id-" + ligne).innerHTML;
    label.innerHTML = valueInput;
    div.appendChild(label);

    fieldset.appendChild(div);

    div = document.createElement("div");
    div.setAttribute("class","pure-control-group center espacement-3-2");

    input = document.createElement("input");
    input.setAttribute("type","button");
    input.setAttribute("id","supprEntr");
    input.setAttribute("class","pure-button pure-button-primary");
    input.setAttribute("onclick","supprEntrepot(" + id + ")");
    input.setAttribute("value","Supprimer");
    div.appendChild(input);

    fieldset.appendChild(div);

    openModal();

}