var ajouterProduitModal = function() {

    document.getElementById('titre-modal').innerHTML = "Ajouter un produit";

    var fieldset = document.getElementById('fieldset-modal');
    fieldset.innerHTML = "";

    div = document.createElement("div");
    div.setAttribute("class", "pure-control-group");

    label = document.createElement("label");
    label.setAttribute("for", "etiquette");
    label.innerHTML = "Nom du produit";
    div.appendChild(label);

    input = document.createElement("input");
    input.setAttribute("id", "etiquette");
    input.setAttribute("type", "text");
    input.setAttribute("placeholder", "nom produit");
    div.appendChild(input);

    fieldset.appendChild(div);

    label = document.createElement("label");
    label.setAttribute("for", "etiquette");
    label.setAttribute("id", "erreur-etiquette");
    label.innerHTML = "Le nom doit avoir au moins 2 caractères";
    label.style.color = "red";
    label.style.display = "none";
    label.style.marginLeft = "9rem";
    fieldset.appendChild(label);

    //
    div = document.createElement("div");
    div.setAttribute("class", "pure-control-group");

    label = document.createElement("label");
    label.setAttribute("for", "code");
    label.innerHTML = "Code du produit";
    div.appendChild(label);

    input = document.createElement("input");
    input.setAttribute("id", "code");
    input.setAttribute("type", "text");
    input.setAttribute("placeholder", "code produit");
    div.appendChild(input);

    fieldset.appendChild(div);

    label = document.createElement("label");
    label.setAttribute("for", "etiquette");
    label.setAttribute("id", "erreur-code");
    label.innerHTML = "Le code doit être un nombre entier de 13 digits";
    label.style.color = "red";
    label.style.display = "none";
    label.style.marginLeft = "9rem";
    fieldset.appendChild(label);

    //
    div = document.createElement("div");
    div.setAttribute("class", "pure-control-group");

    label = document.createElement("label");
    label.setAttribute("for", "caract");
    label.innerHTML = "Caractéristiques du produit";
    div.appendChild(label);

    input = document.createElement("input");
    input.setAttribute("id", "caract");
    input.setAttribute("type", "text");
    input.setAttribute("placeholder", "caractéristiques produit");
    div.appendChild(input);

    fieldset.appendChild(div);

    label = document.createElement("label");
    label.setAttribute("for", "caract");
    label.setAttribute("id", "erreur-cara");
    label.innerHTML = "Veuillez ne pas laisser le champ caractéristiques vide";
    label.style.color = "red";
    label.style.display = "none";
    label.style.marginLeft = "9rem";
    fieldset.appendChild(label);

    //
    div = document.createElement("div");
    div.setAttribute("class", "pure-control-group");

    label = document.createElement("label");
    label.setAttribute("for", "categories-produits");
    label.innerHTML = "Catégorie du produit";
    div.appendChild(label);

    select = document.createElement("select");
    select.setAttribute("name", "categories-produits");
    select.setAttribute("id", "cat-prod");

    for (let i = 0; i < listeCategoriesProduits.length; i++) {

        option = document.createElement("option");
        option.setAttribute("value", listeCategoriesProduits[i].id_categorie_article);
        option.innerHTML = "" + listeCategoriesProduits[i].id_categorie_article + " - " + listeCategoriesProduits[i].etiquette;

        select.appendChild(option);

    }

    div.appendChild(select);
    fieldset.appendChild(div);


    div = document.createElement("div");
    div.setAttribute("class", "pure-control-group center espacement-3-2");

    input = document.createElement("input");
    input.setAttribute("type", "button");
    input.setAttribute("id", "ajouterProd");
    input.setAttribute("class", "pure-button pure-button-primary");
    input.setAttribute("onclick", "ajouterProduit()");
    input.setAttribute("value", "Ajouter");
    div.appendChild(input);

    fieldset.appendChild(div);

    openModal();

}

var editProduitModal = function(id, ligne) {

    document.getElementById('titre-modal').innerHTML = "Modifier le produit #" + id;

    var fieldset = document.getElementById('fieldset-modal');
    fieldset.innerHTML = "";

    div = document.createElement("div");
    div.setAttribute("class", "pure-control-group");

    label = document.createElement("label");
    label.setAttribute("for", "etiquette");
    label.innerHTML = "Nom du produit";
    div.appendChild(label);

    input = document.createElement("input");
    input.setAttribute("id", "etiquette");
    input.setAttribute("type", "text");
    input.setAttribute("placeholder", "nom produit");
    valueInput = document.getElementById("etiquette-" + ligne).innerHTML;
    input.setAttribute("value", valueInput);
    div.appendChild(input);

    fieldset.appendChild(div);

    label = document.createElement("label");
    label.setAttribute("for", "etiquette");
    label.setAttribute("id", "erreur-etiquette");
    label.innerHTML = "Le nom doit avoir au moins 2 caractères";
    label.style.color = "red";
    label.style.display = "none";
    label.style.marginLeft = "9rem";
    fieldset.appendChild(label);

    //
    div = document.createElement("div");
    div.setAttribute("class", "pure-control-group");

    label = document.createElement("label");
    label.setAttribute("for", "code");
    label.innerHTML = "Code du produit";
    div.appendChild(label);

    input = document.createElement("input");
    input.setAttribute("id", "code");
    input.setAttribute("type", "text");
    input.setAttribute("placeholder", "code produit");
    valueInput = document.getElementById("code-" + ligne).innerHTML;
    input.setAttribute("value", valueInput);
    div.appendChild(input);

    fieldset.appendChild(div);

    label = document.createElement("label");
    label.setAttribute("for", "code");
    label.setAttribute("id", "erreur-code");
    label.innerHTML = "Le code doit être un nombre entier de 13 digits";
    label.style.color = "red";
    label.style.display = "none";
    label.style.marginLeft = "9rem";
    fieldset.appendChild(label);

    //
    div = document.createElement("div");
    div.setAttribute("class", "pure-control-group");

    label = document.createElement("label");
    label.setAttribute("for", "code");
    label.innerHTML = "Caractéristiques du produit";
    div.appendChild(label);

    input = document.createElement("input");
    input.setAttribute("id", "caracteristiques");
    input.setAttribute("type", "text");
    input.setAttribute("placeholder", "caractéristiques produit");
    valueInput = document.getElementById("cara-" + ligne).innerHTML;
    input.setAttribute("value", valueInput);
    div.appendChild(input);

    fieldset.appendChild(div);

    label = document.createElement("label");
    label.setAttribute("for", "caracteristiques");
    label.setAttribute("id", "erreur-caracteristiques");
    label.innerHTML = "Veuillez ne pas laisser le champ caractéristiques vide";
    label.style.color = "red";
    label.style.display = "none";
    label.style.marginLeft = "9rem";
    fieldset.appendChild(label);

    //
    div = document.createElement("div");
    div.setAttribute("class", "pure-control-group");

    label = document.createElement("label");
    label.setAttribute("for", "categories-produits");
    label.innerHTML = "Catégorie du produit";
    div.appendChild(label);

    select = document.createElement("select");
    select.setAttribute("name", "categories-produits");
    select.setAttribute("id", "cat-prod");

    for (let i = 0; i < listeCategoriesProduits.length; i++) {

        option = document.createElement("option");
        option.setAttribute("value", listeCategoriesProduits[i].id_categorie_article);
        option.innerHTML = "" + listeCategoriesProduits[i].id_categorie_article + " - " + listeCategoriesProduits[i].etiquette;

        select.appendChild(option);

        if (listeCategoriesProduits[i].id_categorie_article == parseInt(document.getElementById("cat-" + ligne).innerHTML)) {
            select.selectedIndex = i;
        }

    }

    div.appendChild(select);
    fieldset.appendChild(div);


    div = document.createElement("div");
    div.setAttribute("class", "pure-control-group center espacement-3-2");

    input = document.createElement("input");
    input.setAttribute("type", "button");
    input.setAttribute("id", "editProd");
    input.setAttribute("class", "pure-button pure-button-primary");
    console.log("produit Modal id ", id);
    input.setAttribute("onclick", "editProduit(" + id + ")");
    input.setAttribute("value", "Modifier");
    div.appendChild(input);

    fieldset.appendChild(div);

    openModal();

}

var supprProduitModal = function(id, ligne) {

    document.getElementById('titre-modal').innerHTML = "Voulez-vous supprimer le produit #" + id + " ?";

    var fieldset = document.getElementById('fieldset-modal');
    fieldset.innerHTML = "";

    container = document.createElement("div");
    container.setAttribute("class", "center");

    div = document.createElement("div");
    div.setAttribute("class", "pure-control-group");

    label = document.createElement("label");
    label.setAttribute("for", "etiquette");
    label.innerHTML = "Nom du produit";
    label.style.width = "auto";
    div.appendChild(label);

    label = document.createElement("label");
    label.setAttribute("id", "etiquette");
    label.style.width = "auto";
    label.style.fontWeight = "bold";
    valueInput = document.getElementById("etiquette-" + ligne).innerHTML;
    label.innerHTML = valueInput;
    div.appendChild(label);

    container.appendChild(div);

    //
    div = document.createElement("div");
    div.setAttribute("class", "pure-control-group");

    label = document.createElement("label");
    label.setAttribute("for", "code");
    label.style.width = "auto";
    label.innerHTML = "Code du produit";
    div.appendChild(label);

    label = document.createElement("label");
    label.setAttribute("id", "code");
    label.style.width = "auto";
    label.style.fontWeight = "bold";
    valueInput = document.getElementById("code-" + ligne).innerHTML;
    label.innerHTML = valueInput;
    div.appendChild(label);

    container.appendChild(div);

    //
    div = document.createElement("div");
    div.setAttribute("class", "pure-control-group");

    label = document.createElement("label");
    label.setAttribute("for", "code");
    label.style.width = "auto";
    label.innerHTML = "Caractéristiques du produit";
    div.appendChild(label);

    label = document.createElement("label");
    label.setAttribute("id", "caracteristiques");
    label.style.width = "auto";
    label.style.fontWeight = "bold";
    valueInput = document.getElementById("cara-" + ligne).innerHTML;
    label.innerHTML = valueInput;
    div.appendChild(label);

    container.appendChild(div);

    //
    div = document.createElement("div");
    div.setAttribute("class", "pure-control-group");

    label = document.createElement("label");
    label.setAttribute("for", "categories-produits");
    label.innerHTML = "Catégorie du produit";
    label.style.width = "auto";
    div.appendChild(label);

    label = document.createElement("label");
    label.setAttribute("name", "categories-produits");
    label.setAttribute("id", "cat-prod");
    label.style.width = "auto";
    label.style.fontWeight = "bold";

    for (let i = 0; i < listeCategoriesProduits.length; i++) {

        if (listeCategoriesProduits[i].id_categorie_article == parseInt(document.getElementById("cat-" + ligne).innerHTML)) {
            label.innerHTML = listeCategoriesProduits[i].etiquette;
            break;
        }

    }

    div.appendChild(label);
    container.appendChild(div);

    fieldset.appendChild(container);

    div = document.createElement("div");
    div.setAttribute("class", "pure-control-group center espacement-3-2");

    input = document.createElement("input");
    input.setAttribute("type", "button");
    input.setAttribute("id", "supprEntr");
    input.setAttribute("class", "pure-button pure-button-primary");
    input.setAttribute("onclick", "supprProduit(" + id + ")");
    input.setAttribute("value", "Supprimer");
    div.appendChild(input);

    fieldset.appendChild(div);

    openModal();

}