
function getCategorie(idCategorie){
    var options = document.getElementsByTagName("option");
    for (let j = 0; j < options.length; j++) {
        if (idCategorie == parseInt(options[j].value)){
            return options[j].innerText;
        }
    }
}

remplirListe = async function () {
    var categorie = document.getElementById("choix_categorie").value;
    console.log(document.getElementById("choix_categorie").value);
    var url = "";
    if (categorie == 0){
        url = "http://relaxtravel.adrianpaul-carrieres.fr/stock?id_emplacement="+idEmplacement;
    }else{
        url = "http://relaxtravel.adrianpaul-carrieres.fr/stock?id_categorie_article="+categorie+"&id_emplacement="+idEmplacement;
    }
    document.getElementById("contenu_liste").innerHTML="";
    console.log(url);
    var xmlhttp=new XMLHttpRequest();
    xmlhttp.onreadystatechange=function(){
        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
            var articles = JSON.parse(xmlhttp.responseText).data;
            for (let i = 0; i < articles.length; i++) {
                document.getElementById("contenu_liste").innerHTML+="<div id='element_"+articles[i].id_article+"' class=\" m-4 bg-gray-300 hover:bg-gray-400 rounded-lg p-4 flex-wrap  justify-between \">\n" +
                    "                <div class=\"flex \">\n" +
                    "                    <button onclick=\"window.location.href='#article/"+articles[i].id_article+"'\" class=\"w-10 h-10 rounded-full mr-4\" ><i class=\"fas fa-search text-blue-500 text-3xl\"></i></button>\n" +
                    "                    <div class=\"text-sm flex w-full justify-around \">\n" +
                    "                        <div class=\"w-2/3 block  justify-start\">\n" +
                    "                            <span id='etiquette_"+articles[i].id_article+"' class=\" etiquette text-gray-900 font-medium m-2 \">"+articles[i].id_article+"</span>\n" +
                    "                            <span> - </span>"+
                    "                            <span class=\"text-gray-600 font-medium m-2 \"'>"+getCategorie(articles[i].id_categorie_article)+"</span>\n" +
                    "                            <br><span class=\"text-gray-500 m-2 \">"+articles[i].sum+"</span>\n" +
                    "                        </div>\n" +
                    "                        <div class=\"w-1/3 flex justify-end\">\n" +
                    "                            <button onclick=\"window.location.href='#modif-article/"+articles[i].id_article+"'\" class=\" bg-green-400 h-10 text-white my-2 mx-2 px-4 py-2 rounded leading-tight font-medium focus:outline-none focus:bg-blue-300 \"><i class=\"fas fa-pencil-alt\"></i></button>\n" +
                    "                            <button onclick='supprimerListe("+articles[i].id_article+")'  class=\" bg-red-400 text-white h-10  my-2 mx-2 px-4 py-2 rounded leading-tight font-medium focus:outline-none focus:bg-blue-300 \"><i class=\"fas fa-trash\"></i></button>\n" +
                    "                        </div>\n" +
                    "                    </div>\n" +
                    "                </div>\n" +
                    "            </div>";
                console.log(articles[i].sum, articles[i].id_article);
            }
        }
    };
    xmlhttp.open("GET",url,true);
    xmlhttp.send();
};
function getEtiquette() {
    etiquettes = document.getElementsByClassName("etiquette");


        xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.responseText != ""){
                reponse = JSON.parse(xhttp.responseText).data
                console.log(etiquettes.length, reponse.length);
                for (let i = 0; i < etiquettes.length ; i++) {
                    var id = etiquettes[i].innerHTML;
                    for (let j = 0; j < reponse.length; j++) {
                        if (j>id){
                            break;
                        }
                        console.log("i",parseInt(id) ,"j", parseInt(reponse[j].id_article));
                        if (parseInt(id) == parseInt(reponse[j].id_article)){
                            etiquettes[i].innerHTML = reponse[j].etiquette;
                        }
                    }
                }
            }
        };
        xhttp.open("GET","http://relaxtravel.adrianpaul-carrieres.fr/articles", true);
        xhttp.send()
}



var VueListeArticle = (function () {
    var pageListe = document.getElementById("page_liste").innerHTML;
    var header = document.getElementById("header").innerHTML;

    return function () {

        this.afficher = async function () {
            elementBody = document.getElementsByTagName("body")[0];
            elementBody.innerHTML = header + pageListe;
            document.getElementById("contenu_liste").innerHTML = "";
            this.startTime();
            this.getCategories();
            await remplirListe();
            await getEtiquette();
            document.getElementById("choix_categorie").oninput = this.majEtiquette;
        };


        this.majEtiquette = async function() {
            await remplirListe();
            getEtiquette();
        };


        this.getCategories = function () {
            var xmlhttp=new XMLHttpRequest();
            xmlhttp.onreadystatechange=function(){
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    console.log(xmlhttp.responseText);
                    var data = JSON.parse(xmlhttp.responseText);
                    console.log(data);
                    var categories = data.data;
                    console.log(categories);
                    var select = "";
                    console.log(categories);
                    select+="<option value='0' selected>Tous</option>"
                    for (let i = 0; i < categories.length; i++) {
                        select+="<option id='option_"+categories[i].id_categorie_article+"' value='"+categories[i].id_categorie_article+"'>"+categories[i].etiquette+"</option>"
                    }
                    document.getElementById("choix_categorie").innerHTML = select;


                }
            };
            xmlhttp.open("GET","http://relaxtravel.adrianpaul-carrieres.fr/categories-article",true);
            xmlhttp.send();
        };

        this.startTime = function () {
            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById("heure").innerHTML = h;
            document.getElementById("minute").innerHTML = m;
            document.getElementById("seconde").innerHTML = s;


            var t = setTimeout(startTime, 500);
        }
    }
})();