var VueUtilisateur = (function () {

    emplacement = document.getElementById('emplacement-donnees');
    titre = document.getElementById('titre');

    return function () {

        this.afficher = function () {

            var xmlhttp = new XMLHttpRequest();
           
            xmlhttp.onreadystatechange = function() {
        
                if (this.readyState == 4 && this.status == 200) {

                    jsonData = JSON.parse(this.responseText);
                    // console.log(jsonData.data);

                    listeCategoriesUtilisateurs = [];

                    for (let i = 0; i < jsonData.data.length; i++) {
                        listeCategoriesUtilisateurs.push(new CategorieUtilisateur(jsonData.data[i].id_type_utilisateur, jsonData.data[i].etiquette));
                    }                    

                    var xmlhttp2 = new XMLHttpRequest();
                
                    xmlhttp2.onreadystatechange = function() {
                        
                        if (this.readyState == 4 && this.status == 200) {
                
                            // alert(this.responseText);
                            jsonData = JSON.parse(this.responseText);
                            // console.log(jsonData.data);
                            
                            emplacement.innerHTML = "";
                            titre.innerHTML = "Liste des utilisateurs";

                            table = document.createElement("table");
                            table.setAttribute("class", "pure-table pure-table-horizontal");
                            table.setAttribute("id", "table");

                            thead = document.createElement("thead");
                            tr = document.createElement("tr");

                            id = document.createElement("th");
                            id.innerHTML = "#Id Utlisateur";
                            tr.appendChild(id);

                            code = document.createElement("th");
                            code.innerHTML = "Code";
                            tr.appendChild(code);
                            
                            nom = document.createElement("th");
                            nom.innerHTML = "Identifiant";
                            tr.appendChild(nom);

                            cara = document.createElement("th");
                            cara.innerHTML = "Droits";
                            tr.appendChild(cara);

                            suppr = document.createElement("th");
                            suppr.innerHTML = "Modification";
                            tr.appendChild(suppr);

                            thead.appendChild(tr);
                            table.appendChild(thead);

                            tbody = document.createElement("tbody");
                            tbody.setAttribute("id","table-body");

                            for (let i = 0; i < jsonData.data.length; i++) {

                                tr = document.createElement("tr");
                                td = document.createElement("td");
                                td.style.fontWeight = "bold";
                                td.innerHTML = '<a onclick="consulterUtilisateurModal('+ jsonData.data[i].id_utilisateur + ',' + i + ')" class="acces-button acces-button-a">' + jsonData.data[i].id_utilisateur + '</a> '
                                tr.appendChild(td);

                                td = document.createElement("td");
                                td.setAttribute("id","code-" + i);
                                td.innerHTML = jsonData.data[i].code;
                                tr.appendChild(td);

                                td = document.createElement("td");
                                td.setAttribute("id","identifiant-" + i);
                                td.innerHTML = jsonData.data[i].identifiant;
                                tr.appendChild(td);

                                td = document.createElement("td");
                                td.setAttribute("id","type-utilisateur-" + i);
                                td.innerHTML = jsonData.data[i].id_type_utilisateur;
                                tr.appendChild(td);

                                td = document.createElement("td");
                                td.innerHTML = '<input type="button" onclick="editUtilisateurModal('+ jsonData.data[i].id_utilisateur + ',' + i + ')" value="&#xf044" class="fa fa-input acces-button"><input type="button" onclick="supprUtilisateurModal('+ jsonData.data[i].id_utilisateur + ',' + i + ')" value="&#xf1f8" class="fa fa-input acces-button">';
                                tr.appendChild(td);

                                tbody.appendChild(tr);
                                
                            }

                            table.appendChild(tbody);
                            
                            emplacement.appendChild(table);

                            ajouter = document.createElement("button");
                            ajouter.setAttribute("class","pure-button pure-button-primary espacement-2");
                            ajouter.setAttribute("onclick","ajouterUtilisateurModal()");
                            
                            ajouter.innerHTML = "Ajouter un utilisateur";

                            emplacement.appendChild(ajouter);

                        }
                
                    };
                    xmlhttp2.open("GET", "http://relaxtravel.adrianpaul-carrieres.fr/utilisateurs", true);
                    xmlhttp2.send();

                }
                
            };
            xmlhttp.open("GET", "http://relaxtravel.adrianpaul-carrieres.fr/types-utilisateur", true);
            xmlhttp.send();
            
        }

    }

})();