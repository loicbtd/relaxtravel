var VueProduits = (function () {

    emplacement = document.getElementById('emplacement-donnees');
    titre = document.getElementById('titre');

    return function () {

        this.afficher = function () {

            var xmlhttp = new XMLHttpRequest();
           
            xmlhttp.onreadystatechange = function() {
        
                if (this.readyState == 4 && this.status == 200) {

                    jsonData = JSON.parse(this.responseText);
                    // console.log(jsonData.data);

                    listeCategoriesProduits = [];

                    for (let i = 0; i < jsonData.data.length; i++) {
                        listeCategoriesProduits.push(new CategorieProduit(jsonData.data[i].id_categorie_article, jsonData.data[i].etiquette));
                    }                    

                    var xmlhttp2 = new XMLHttpRequest();
        
                    xmlhttp2.onreadystatechange = function() {
                
                        
                        if (this.readyState == 4 && this.status == 200) {
                
                            // alert(this.responseText);
                            jsonData = JSON.parse(this.responseText);
                            // console.log(jsonData.data);

                            emplacement.innerHTML = "";
                            titre.innerHTML = "Liste de tous les produits";

                            table = document.createElement("table");
                            table.setAttribute("class", "pure-table pure-table-horizontal");
                            table.setAttribute("id", "table");

                            thead = document.createElement("thead");
                            tr = document.createElement("tr");

                            id = document.createElement("th");
                            id.innerHTML = "#Id Produit";
                            tr.appendChild(id);

                            code = document.createElement("th");
                            code.innerHTML = "Code";
                            tr.appendChild(code);
                            
                            nom = document.createElement("th");
                            nom.innerHTML = "Dénommination";
                            tr.appendChild(nom);

                            cara = document.createElement("th");
                            cara.innerHTML = "Caractéristiques";
                            tr.appendChild(cara);

                            id = document.createElement("th");
                            id.innerHTML = "#Id Catégorie";
                            tr.appendChild(id);

                            suppr = document.createElement("th");
                            suppr.innerHTML = "Modification";
                            tr.appendChild(suppr);

                            thead.appendChild(tr);
                            table.appendChild(thead);

                            tbody = document.createElement("tbody");
                            tbody.setAttribute("id","table-body");

                            for (let i = 0; i < jsonData.data.length; i++) {
                                
                                tr = document.createElement("tr");
                                td = document.createElement("td");
                                td.setAttribute("class","id-tableau");
                                td.innerHTML = "" + jsonData.data[i].id_article;
                                tr.appendChild(td);

                                td = document.createElement("td");
                                td.setAttribute("id","code-" + i);
                                td.innerHTML = jsonData.data[i].code;
                                tr.appendChild(td);

                                td = document.createElement("td");
                                td.setAttribute("id","etiquette-" + i);
                                td.innerHTML = jsonData.data[i].etiquette;
                                tr.appendChild(td);

                                td = document.createElement("td");
                                td.setAttribute("id","cara-" + i);
                                td.innerHTML = jsonData.data[i].caracteristiques;
                                tr.appendChild(td);

                                td = document.createElement("td");
                                td.setAttribute("id","cat-" + i);
                                td.innerHTML = jsonData.data[i].id_categorie_article;
                                tr.appendChild(td);

                                td = document.createElement("td");
                                td.innerHTML = '<input type="button" onclick="editProduitModal('+ jsonData.data[i].id_article + ',' + i + ')" value="&#xf044" class="fa fa-input acces-button"><input type="button" onclick="supprProduitModal('+ jsonData.data[i].id_article  + ',' + i + ')" value="&#xf1f8" class="fa fa-input acces-button">';
                                tr.appendChild(td);

                                tbody.appendChild(tr);
                                
                            }
                            
                            table.appendChild(tbody);
                            
                            emplacement.appendChild(table);

                            ajouter = document.createElement("button");
                            ajouter.setAttribute("class","pure-button pure-button-primary espacement-2");
                            ajouter.setAttribute("onclick","ajouterProduitModal()");
                            
                            ajouter.innerHTML = "Ajouter un produit";

                            emplacement.appendChild(ajouter);
                
                        }
                
                    };
                    xmlhttp2.open("GET", "http://relaxtravel.adrianpaul-carrieres.fr/articles", true);
                    xmlhttp2.send();

                }
        
            };
            xmlhttp.open("GET", "http://relaxtravel.adrianpaul-carrieres.fr/categories-article", true);
            xmlhttp.send();
 
        }

    }

})();