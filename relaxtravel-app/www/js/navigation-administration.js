var accueil = new VueAccueilAdministration();
var entrepots = new VueEntrepot();
var produits = new VueProduits();
var categorieProduit = new VueCategorieProduit();
var categorieUtilisateur = new VueCategorieUtilisateur();
var utilisateurs = new VueUtilisateur();

var listeCategoriesProduits = [];
var listeCategoriesUtilisateurs = [];

(function() {

    var initialiser = function initialiser() {

        window.addEventListener("hashchange", naviguer);
        
        naviguer();

    };

    var naviguer = function() {

        var hash = window.location.hash;

        if(!hash) {

            accueil.afficher();

        } else if(hash.match(/^#Entrepots/)) {

            entrepots.afficher();
            
        } else if(hash.match(/^#Produits/)) {

            produits.afficher();
            
        } else if(hash.match(/^#Categories-Produits/)) {
            
            categorieProduit.afficher();
            
        } else if(hash.match(/^#Categories-Utilisateurs/)){

            categorieUtilisateur.afficher();

        } else if(hash.match(/^#Utilisateurs/)){

            utilisateurs.afficher();

        }
        
    }

    initialiser();

})();