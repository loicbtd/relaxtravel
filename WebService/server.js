//Express
let express = require('express');
let app = express();
let bodyParser = require('body-parser');
let cors = require('cors');

//Routes
let routeEmplacement = require('./routes/routeEmplacement');
let routeCategorieArticle = require('./routes/routeCategorieArticle');
let routeArticle = require('./routes/routeArticle');
let routeTypeTransaction = require('./routes/routeTypeTransaction');
let routeTypeUtilisateur = require('./routes/routeTypeUtilisateur');
let routeUtilisateur = require('./routes/routeUtilisateur');
let routeHistoriqueStock = require('./routes/routeHistoriqueStock');
let routeStock = require('./routes/routeStock');

//Fichier de config
let config = require('config');

//Logs
let morgan = require('morgan');

//Pas besoin de log quand on fait des tests !
if (config.util.getEnv('NODE_ENV') != 'test') {
    app.use(morgan('combined'));
    port = 8080;
} else {
    port = 8081;
}

//parse application/json and look for raw text                                        
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.text());
app.use(bodyParser.json({ type: 'application/json' }));
app.use(cors())

//Début du programme !
console.log("Routing...");

/*
 * Gestion des emplacements
 */
app.route("/emplacements")
    .get(routeEmplacement.getEmplacements)
    .post(routeEmplacement.postEmplacement)
app.route("/emplacements/:id")
    .get(routeEmplacement.getEmplacement)
    .delete(routeEmplacement.deleteEmplacement)
    .put(routeEmplacement.updateEmplacement)

/*
 * Gestion des catégories des articles
 */
app.route("/categories-article")
    .get(routeCategorieArticle.getCategoriesArticle)
    .post(routeCategorieArticle.postCategorieArticle)
app.route("/categories-article/:id")
    .get(routeCategorieArticle.getCategorieArticle)
    .delete(routeCategorieArticle.deleteCategorieArticle)
    .put(routeCategorieArticle.updateCategorieArticle)

/*
 * Gestion des articles
 */
app.route("/articles")
    .get(routeArticle.getArticles)
    .post(routeArticle.postArticle)
app.route("/articles/:id")
    .get(routeArticle.getArticle)
    .delete(routeArticle.deleteArticle)
    .put(routeArticle.updateArticle)
app.route("/articles-code/:code")
    .get(routeArticle.getArticleParCode)


/*
 * Gestion des transactions
 */
app.route("/types-transaction")
    .get(routeTypeTransaction.getTypesTransaction)
    .post(routeTypeTransaction.postTypeTransaction)
app.route("/types-transaction/:id")
    .get(routeTypeTransaction.getTypeTransaction)
    .delete(routeTypeTransaction.deleteTypeTransaction)
    .put(routeTypeTransaction.updateTypeTransaction)

/*
 * Gestion des types utilisateurs
 */
app.route("/types-utilisateur")
    .get(routeTypeUtilisateur.getTypesUtilisateur)
    .post(routeTypeUtilisateur.postTypeUtilisateur)
app.route("/types-utilisateur/:id")
    .get(routeTypeUtilisateur.getTypeUtilisateur)
    .delete(routeTypeUtilisateur.deleteTypeUtilisateur)
    .put(routeTypeUtilisateur.updateTypeUtilisateur)

/*
 * Gestion des utilisateurs
 */
app.route("/utilisateurs")
    .get(routeUtilisateur.getUtilisateurs)
    .post(routeUtilisateur.postUtilisateur)
app.route("/utilisateurs/:id")
    .get(routeUtilisateur.getUtilisateur)
    .delete(routeUtilisateur.deleteUtilisateur)
    .put(routeUtilisateur.updateUtilisateur)

/*
 * Gestion des historiques stock
 */
app.route("/historiques-stock")
    .get(routeHistoriqueStock.getHistoriquesStock)
    .post(routeHistoriqueStock.postHistoriqueStock)
app.route("/historiques-stock/:id")
    .get(routeHistoriqueStock.getHistoriqueStock)

app.route("/stock").get(routeStock.getStock)

app.listen(port);

console.log("Listening on port " + port);
module.exports = app; // for testing