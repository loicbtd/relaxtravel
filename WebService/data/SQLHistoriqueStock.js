SQL_AJOUTER = `
        INSERT INTO historique_stock (id_article, id_utilisateur, id_emplacement, date_saisie, quantite, id_type_transaction) VALUES ($1,$2,$3,NOW(),$4,$5) RETURNING id_historique_stock
`;

SQL_LISTER = `
    SELECT id_historique_stock, id_article, id_utilisateur, id_emplacement, date_saisie, quantite, id_type_transaction 
    FROM historique_stock 
    ORDER BY id_historique_stock ASC
`;

SQL_TROUVER_PAR_ID = `
    SELECT id_historique_stock, id_article, id_utilisateur, id_emplacement, date_saisie, quantite, id_type_transaction 
    FROM historique_stock 
    WHERE id_historique_stock=$1    
`;

module.exports = { SQL_AJOUTER, SQL_LISTER, SQL_TROUVER_PAR_ID };