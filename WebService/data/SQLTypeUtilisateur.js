SQL_AJOUTER = "INSERT INTO type_utilisateur (etiquette) VALUES ($1) RETURNING id_type_utilisateur";
SQL_LISTER = "SELECT id_type_utilisateur, etiquette FROM type_utilisateur ORDER BY id_type_utilisateur ASC";
SQL_TROUVER_PAR_ID = "SELECT id_type_utilisateur, etiquette FROM type_utilisateur WHERE id_type_utilisateur=$1";
SQL_MODIFIER = "UPDATE type_utilisateur SET etiquette=$2 WHERE id_type_utilisateur=$1";
SQL_SUPPRIMER_PAR_ID = "DELETE FROM type_utilisateur WHERE id_type_utilisateur=$1";

module.exports = { SQL_AJOUTER, SQL_LISTER, SQL_TROUVER_PAR_ID, SQL_MODIFIER, SQL_SUPPRIMER_PAR_ID };