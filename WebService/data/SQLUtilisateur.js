SQL_AJOUTER = "INSERT INTO utilisateur (code, id_type_utilisateur, identifiant, mot_de_passe) VALUES ($1, $2, $3, $4) RETURNING id_utilisateur";
SQL_LISTER = "SELECT id_utilisateur, code, id_type_utilisateur, identifiant, mot_de_passe FROM utilisateur ORDER BY id_utilisateur ASC";
SQL_TROUVER_PAR_ID = "SELECT id_utilisateur, code, id_type_utilisateur, identifiant, mot_de_passe FROM utilisateur WHERE id_utilisateur=$1";
SQL_MODIFIER = "UPDATE utilisateur SET code=$2, id_type_utilisateur=$3, identifiant=$4, mot_de_passe=$5 WHERE id_utilisateur=$1";
SQL_SUPPRIMER_PAR_ID = "DELETE FROM utilisateur WHERE id_utilisateur=$1";

module.exports = { SQL_AJOUTER, SQL_LISTER, SQL_TROUVER_PAR_ID, SQL_MODIFIER, SQL_SUPPRIMER_PAR_ID };