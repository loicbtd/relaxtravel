SQL_AJOUTER = "INSERT INTO emplacement (etiquette) VALUES ($1) RETURNING id_emplacement";
SQL_LISTER = "SELECT id_emplacement, etiquette FROM emplacement ORDER BY id_emplacement ASC";
SQL_TROUVER_PAR_ID = "SELECT id_emplacement, etiquette FROM emplacement WHERE id_emplacement=$1";
SQL_MODIFIER = "UPDATE emplacement SET etiquette=$2 WHERE id_emplacement=$1";
SQL_SUPPRIMER_PAR_ID = "DELETE FROM emplacement WHERE id_emplacement=$1";

module.exports = { SQL_AJOUTER, SQL_LISTER, SQL_TROUVER_PAR_ID, SQL_MODIFIER, SQL_SUPPRIMER_PAR_ID };