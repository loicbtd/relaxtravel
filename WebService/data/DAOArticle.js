const BaseDeDonnees = require('../data/BaseDeDonnees');
const SQLArticle = require('./SQLArticle');

ajouter = async function(article) {
    let baseDeDonnees = BaseDeDonnees.getInstance();
    let parametres_requete = [article.code, article.id_categorie_article, article.etiquette, article.caracteristiques];
    try {
        await baseDeDonnees.query('BEGIN');
        let resultat = await baseDeDonnees.query(SQLArticle.SQL_AJOUTER, parametres_requete);
        await baseDeDonnees.query('COMMIT');
        if (resultat.rowCount > 0) {
            return [true, Object.values(resultat.rows[0])[0]];
        } else {
            return [false, "Aucun object affecté"];
        }
    } catch (erreur) {
        await baseDeDonnees.query('ROLLBACK');
        return [false, erreur.stack];
    }
}

lister = async function() {
    let baseDeDonnees = BaseDeDonnees.getInstance();
    try {
        let enregistrements = (await baseDeDonnees.query(SQLArticle.SQL_LISTER)).rows;
        return [true, enregistrements];
    } catch (erreur) {
        return [false, erreur.stack];
    }
}

recupererParId = async function(id) {
    let baseDeDonnees = BaseDeDonnees.getInstance();
    let parametres_requete = [id];
    try {
        let enregistrements = (await baseDeDonnees.query(SQLArticle.SQL_TROUVER_PAR_ID, parametres_requete)).rows;
        if (enregistrements.length != 0) {
            return [true, enregistrements];
        } else {
            return [false, "Il n'existe pas d'objet avec cet id"];
        }
    } catch (erreur) {
        return [false, erreur.stack];
    }
}

recupererParCodeBarre = async function(valeur) {
    let baseDeDonnees = BaseDeDonnees.getInstance();
    let parametres_requete = [valeur.code];
    try {
        let enregistrements = (await baseDeDonnees.query(SQLArticle.SQL_TROUVER_PAR_CODE, parametres_requete)).rows;
        if (enregistrements.length != 0) {
            return [true, enregistrements];
        } else {
            return [false, "Il n'existe pas d'objet avec ces paramètres."];
        }
    } catch (erreur) {
        return [false, erreur.stack];
    }
}

modifier = async function(article) {
    let baseDeDonnees = BaseDeDonnees.getInstance();
    let parametres_requete = [article.id_article, article.code, article.id_categorie_article, article.etiquette, article.caracteristiques];
    try {
        await baseDeDonnees.query('BEGIN');
        let resultat = await baseDeDonnees.query(SQLArticle.SQL_MODIFIER, parametres_requete);
        await baseDeDonnees.query('COMMIT');
        if (resultat.rowCount > 0) {
            return [true, "Modification réussie"];
        } else {
            return [false, "Aucun object affecté"];
        }
    } catch (erreur) {
        await baseDeDonnees.query('ROLLBACK');
        return [false, erreur.stack];
    }
}

supprimerParId = async function(id) {
    let baseDeDonnees = BaseDeDonnees.getInstance();
    let parametres_requete = [id];
    try {
        await baseDeDonnees.query('BEGIN');
        let resultat = await baseDeDonnees.query(SQLArticle.SQL_SUPPRIMER_PAR_ID, parametres_requete);
        await baseDeDonnees.query('COMMIT');
        if (resultat.rowCount > 0) {
            return [true, "Suppression réussie"];
        } else {
            return [false, "Aucun object affecté"];
        }
    } catch (erreur) {
        await baseDeDonnees.query('ROLLBACK');
        return [false, erreur.stack];
    }
}

module.exports = { ajouter, lister, recupererParId, recupererParCodeBarre, modifier, supprimerParId };