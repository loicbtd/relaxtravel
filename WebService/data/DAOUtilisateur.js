const BaseDeDonnees = require('../data/BaseDeDonnees');
const SQLUtilisateur = require('./SQLUtilisateur');

ajouter = async function(utilisateur) {
    let baseDeDonnees = BaseDeDonnees.getInstance();
    let parametres_requete = [utilisateur.code, utilisateur.id_type_utilisateur, utilisateur.identifiant, utilisateur.mot_de_passe];
    try {
        await baseDeDonnees.query('BEGIN');
        let resultat = await baseDeDonnees.query(SQLUtilisateur.SQL_AJOUTER, parametres_requete);
        await baseDeDonnees.query('COMMIT');
        if (resultat.rowCount > 0) {
            return [true, Object.values(resultat.rows[0])[0]];
        } else {
            return [false, "Aucun object affecté"];
        }
    } catch (erreur) {
        await baseDeDonnees.query('ROLLBACK');
        return [false, erreur.stack];
    }
}

lister = async function() {
    let baseDeDonnees = BaseDeDonnees.getInstance();
    try {
        let enregistrements = (await baseDeDonnees.query(SQLUtilisateur.SQL_LISTER)).rows;
        return [true, enregistrements];
    } catch (erreur) {
        return [false, erreur.stack];
    }
}

recupererParId = async function(id) {
    let baseDeDonnees = BaseDeDonnees.getInstance();
    let parametres_requete = [id];
    try {
        let enregistrements = (await baseDeDonnees.query(SQLUtilisateur.SQL_TROUVER_PAR_ID, parametres_requete)).rows;
        if (enregistrements.length != 0) {
            return [true, enregistrements];
        } else {
            return [false, "Il n'existe pas d'objet avec cet id"];
        }
    } catch (erreur) {
        return [false, erreur.stack];
    }
}

modifier = async function(utilisateur) {
    let baseDeDonnees = BaseDeDonnees.getInstance();
    let parametres_requete = [utilisateur.id_utilisateur, utilisateur.code, utilisateur.id_type_utilisateur, utilisateur.identifiant, utilisateur.mot_de_passe];
    try {
        await baseDeDonnees.query('BEGIN');
        let resultat = await baseDeDonnees.query(SQLUtilisateur.SQL_MODIFIER, parametres_requete);
        await baseDeDonnees.query('COMMIT');
        if (resultat.rowCount > 0) {
            return [true, "Modification réussie"];
        } else {
            return [false, "Aucun object affecté"];
        }
    } catch (erreur) {
        await baseDeDonnees.query('ROLLBACK');
        return [false, erreur.stack];
    }
}

supprimerParId = async function(id) {
    let baseDeDonnees = BaseDeDonnees.getInstance();
    let parametres_requete = [id];
    try {
        await baseDeDonnees.query('BEGIN');
        let resultat = await baseDeDonnees.query(SQLUtilisateur.SQL_SUPPRIMER_PAR_ID, parametres_requete);
        await baseDeDonnees.query('COMMIT');
        if (resultat.rowCount > 0) {
            return [true, "Suppression réussie"];
        } else {
            return [false, "Aucun object affecté"];
        }
    } catch (erreur) {
        await baseDeDonnees.query('ROLLBACK');
        return [false, erreur.stack];
    }
}

module.exports = { ajouter, lister, recupererParId, modifier, supprimerParId };