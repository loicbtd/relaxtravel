const BaseDeDonnees = require('./BaseDeDonnees');
const SQLStock = require('./SQLStock');

lister = async function(parametres) {
    let baseDeDonnees = BaseDeDonnees.getInstance();
    try {
        let enregistrements = (await baseDeDonnees.query(SQLStock.SQL_LISTER(parametres))).rows;
        if (enregistrements.length != 0) {
            return [true, enregistrements];
        } else {
            return [false, "Il n'existe pas d'objet repondant aux critères spécifiés."];
        }
    } catch (erreur) {
        return [false, erreur.stack];
    }
}

module.exports = { lister };