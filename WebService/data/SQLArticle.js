SQL_AJOUTER = "INSERT INTO article (code, id_categorie_article, etiquette, caracteristiques) VALUES ($1, $2, $3, $4) RETURNING id_article";
SQL_LISTER = "SELECT id_article, code, id_categorie_article, etiquette, caracteristiques FROM article ORDER BY id_article ASC";

SQL_TROUVER_PAR_ID = "SELECT id_article, code, id_categorie_article, etiquette, caracteristiques FROM article WHERE id_article=$1";

SQL_TROUVER_PAR_CODE = "SELECT id_article, code, id_categorie_article, etiquette, caracteristiques FROM article WHERE code=$1";

SQL_MODIFIER = "UPDATE article SET code=$2, id_categorie_article=$3, etiquette=$4, caracteristiques=$5 WHERE id_article=$1";
SQL_SUPPRIMER_PAR_ID = "DELETE FROM article WHERE id_article=$1";

module.exports = { SQL_AJOUTER, SQL_LISTER, SQL_TROUVER_PAR_ID, SQL_TROUVER_PAR_CODE, SQL_MODIFIER, SQL_SUPPRIMER_PAR_ID };