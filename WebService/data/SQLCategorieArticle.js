SQL_AJOUTER = "INSERT INTO categorie_article (etiquette) VALUES ($1) RETURNING id_categorie_article";
SQL_LISTER = "SELECT id_categorie_article, etiquette FROM categorie_article ORDER BY id_categorie_article ASC";
SQL_TROUVER_PAR_ID = "SELECT id_categorie_article, etiquette FROM categorie_article WHERE id_categorie_article=$1";
SQL_MODIFIER = "UPDATE categorie_article SET etiquette=$2 WHERE id_categorie_article=$1";
SQL_SUPPRIMER_PAR_ID = "DELETE FROM categorie_article WHERE id_categorie_article=$1";

module.exports = { SQL_AJOUTER, SQL_LISTER, SQL_TROUVER_PAR_ID, SQL_MODIFIER, SQL_SUPPRIMER_PAR_ID };