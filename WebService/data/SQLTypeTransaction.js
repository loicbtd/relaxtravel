SQL_AJOUTER = "INSERT INTO type_transaction (etiquette) VALUES ($1) RETURNING id_type_transaction";
SQL_LISTER = "SELECT id_type_transaction, etiquette FROM type_transaction ORDER BY id_type_transaction ASC";
SQL_TROUVER_PAR_ID = "SELECT id_type_transaction, etiquette FROM type_transaction WHERE id_type_transaction=$1";
SQL_MODIFIER = "UPDATE type_transaction SET etiquette=$2 WHERE id_type_transaction=$1";
SQL_SUPPRIMER_PAR_ID = "DELETE FROM type_transaction WHERE id_type_transaction=$1";

module.exports = { SQL_AJOUTER, SQL_LISTER, SQL_TROUVER_PAR_ID, SQL_MODIFIER, SQL_SUPPRIMER_PAR_ID };