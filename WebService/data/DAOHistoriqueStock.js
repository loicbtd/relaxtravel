const BaseDeDonnees = require('../data/BaseDeDonnees');
const SQLHistoriqueStock = require('./SQLHistoriqueStock');

ajouter = async function(historiqueStock) {
    let baseDeDonnees = BaseDeDonnees.getInstance();
    let parametres_requete = [historiqueStock.id_article, historiqueStock.id_utilisateur, historiqueStock.id_emplacement, historiqueStock.quantite, historiqueStock.id_type_transcation];
    try {
        await baseDeDonnees.query('BEGIN');
        let resultat = await baseDeDonnees.query(SQLHistoriqueStock.SQL_AJOUTER, parametres_requete);
        await baseDeDonnees.query('COMMIT');
        if (resultat.rowCount > 0) {
            return [true, Object.values(resultat.rows[0])[0]];
        } else {
            return [false, "Aucun object affecté"];
        }
    } catch (erreur) {
        await baseDeDonnees.query('ROLLBACK');
        return [false, erreur.stack];
    }
}

lister = async function() {
    let baseDeDonnees = BaseDeDonnees.getInstance();
    try {
        let enregistrements = (await baseDeDonnees.query(SQLHistoriqueStock.SQL_LISTER)).rows;
        return [true, enregistrements];
    } catch (erreur) {
        return [false, erreur.stack];
    }
}

recupererParId = async function(id) {
    let baseDeDonnees = BaseDeDonnees.getInstance();
    let parametres_requete = [id];
    try {
        let enregistrements = (await baseDeDonnees.query(SQLHistoriqueStock.SQL_TROUVER_PAR_ID, parametres_requete)).rows;
        if (enregistrements.length != 0) {
            return [true, enregistrements];
        } else {
            return [false, "Il n'existe pas d'objet avec cet id"];
        }
    } catch (erreur) {
        return [false, erreur.stack];
    }
}

module.exports = { ajouter, lister, recupererParId };