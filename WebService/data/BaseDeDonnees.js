const Client = require('pg').Client;

let instance;

getInstance = function() {
    if (instance) {
        return instance;
    }
    instance = new Client({
        host: 'homebert.fr',
        user: 'relaxtravel',
        password: 'password',
        database: 'relaxtravel',
        port: 2232
    });
    instance.connect();
    return instance;
}

module.exports = { getInstance };