SQL_LISTER = function(parametres) {

    parametres.id_article = !parametres.id_article ? "historique_stock.id_article" : parametres.id_article;
    parametres.id_categorie_article = !parametres.id_categorie_article ? "article.id_categorie_article" : parametres.id_categorie_article;
    parametres.id_emplacement = !parametres.id_emplacement ? "historique_stock.id_emplacement" : parametres.id_emplacement;
    parametres.id_type_transaction = !parametres.id_type_transaction ? "historique_stock.id_type_transaction" : parametres.id_type_transaction;
    parametres.id_type_utilisateur = !parametres.id_type_utilisateur ? "utilisateur.id_type_utilisateur" : parametres.id_type_utilisateur;
    parametres.id_utilisateur = !parametres.id_utilisateur ? "historique_stock.id_utilisateur" : parametres.id_utilisateur;

    return `
        SELECT id_article,
        id_categorie_article,
        SUM(quantite)
        FROM (
            SELECT historique_stock.id_article, 
                historique_stock.id_utilisateur, 
                historique_stock.id_emplacement, 
                historique_stock.quantite , 
                historique_stock.id_type_transaction,
                utilisateur.id_type_utilisateur,
                article.id_categorie_article
            FROM historique_stock
            INNER JOIN article ON historique_stock.id_article = article.id_article
            INNER JOIN utilisateur ON historique_stock.id_utilisateur = utilisateur.id_utilisateur
            WHERE historique_stock.id_article=` + parametres.id_article + `
                AND article.id_categorie_article=` + parametres.id_categorie_article + `
                AND historique_stock.id_emplacement=` + parametres.id_emplacement + `
                AND historique_stock.id_type_transaction=` + parametres.id_type_transaction + `
                AND utilisateur.id_type_utilisateur=` + parametres.id_type_utilisateur + `
                AND historique_stock.id_utilisateur=` + parametres.id_utilisateur + `
            ORDER BY article.id_categorie_article ASC
        ) AS historique_stock
        GROUP BY id_article, id_categorie_article
    `;
}

module.exports = { SQL_LISTER };