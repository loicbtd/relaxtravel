//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let DAOEmplacement = require('../data/DAOEmplacement');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();

chai.use(chaiHttp);

//Bloc parent des tests
describe('Emplacement', () => {
    beforeEach((done) => {
        DAOEmplacement.liste = [];
        done();
    });

    /*
     * Test de la route /GET
     */
    describe('/GET emplacements', () => {
        it("Cela devrait obtenir tous les emplacements", (done) => {
            chai.request(server)
                .get('/emplacements')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.status.should.be.eql('success');
                    res.body.data.length.should.not.be.eql(0);
                    done();
                })
        })
    });

    describe('/POST emplacements', () => {
        it("On ne peut pas poster un emplacement s'il manque l'etiquette", (done) => {
            let emplacement = { "id_emplacement": 1 };

            chai.request(server)
                .post('/emplacements')
                .send(emplacement)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.be.a('object');
                    res.body.status.should.be.eql("error");
                    res.body.message.should.be.eql('"etiquette" is required')
                    done();
                })
        })
        it("On peut créer un nouvel emplacement si toutes les informations sont correctes et nécessaires", (done) => {
            let emplacement = { "etiquette": "emplacement 1" };

            chai.request(server)
                .post('/emplacements')
                .send(emplacement)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.status.should.be.eql("success");
                    done();
                })
        })
    });

    describe('/GET/id emplacement', () => {
        it("Cela ne devrait pas obtenir d'emplacement avec une id négative", (done) => {
            chai.request(server)
                .get('/emplacements/-1')
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.be.a('object');
                    res.body.status.should.be.eql("error");
                    res.body.message.should.be.eql("Erreur dans la requête");
                    done();
                })
        })
        it("On ne peut pas obtenir un emplacement s'il n'existe pas", (done) => {
            chai.request(server)
                .get('/emplacements/10')
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.be.a('object');
                    res.body.status.should.be.eql("error");
                    res.body.message.should.be.eql("Objet inexistant")
                    done();
                })
        })
        it("Cela devrait retourner un seul emplacement", (done) => {
            emplacement = { "id_emplacement": 1, "etiquette": "emplacement 1" };
            DAOEmplacement.ajouter(emplacement);
            chai.request(server)
                .get('/emplacements/1')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.status.should.be.eql("success");
                    res.body.data.should.be.eql({ "id_emplacement": 1, "etiquette": "emplacement 1" })
                    done();
                })
        })
    });

    describe('/PUT emplacement', () => {
        it("On ne peut pas modifier un emplacement si le etiquette est vide", (done) => {
            let emplacement = { "id_emplacement": 1, "etiquette": "" };

            chai.request(server)
                .put('/emplacements/1')
                .send(emplacement)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.be.a('object');
                    res.body.status.should.be.eql("error")
                    res.body.message.should.be.eql('"etiquette" is not allowed to be empty')
                    done();
                })
        })
        it("On ne peut pas modifier un emplacement s'il n'existe pas", (done) => {
            let emplacement = { "id_emplacement": 10, "etiquette": "emplacement modifié" };

            chai.request(server)
                .put('/emplacements/1')
                .send(emplacement)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.be.a('object');
                    res.body.status.should.be.eql("error")
                    res.body.message.should.be.eql("Aucun objet affecté")
                    done();
                })
        })
        it("On peut modifier un emplacement si toutes les valeurs sont valides", (done) => {
            let emplacement = { "id_emplacement": 1, "etiquette": "emplacement modifié" };

            chai.request(server)
                .put('/emplacements/1')
                .send(emplacement)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql("Modification réussie")
                    done();
                })
        })
    });

    describe('/DELETE/id emplacement', () => {
        it("Cela ne devrait pas supprimer d'emplacement avec une id qui n'est pas un nombre", (done) => {
            let id = 'a';

            chai.request(server)
                .delete('/emplacements/' + id)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.be.a('object');
                    res.body.status.should.be.eql("error");
                    res.body.message.should.be.eql("Erreur dans la requête");
                    done();
                })
        })
        it("On ne peut pas supprimer un emplacement s'il n'existe pas", (done) => {
            let emplacement = { "id_emplacement": 10, "etiquette": "emplacement modifié" };

            chai.request(server)
                .delete('/emplacements/10')
                .send(emplacement)
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.be.a('object');
                    res.body.status.should.be.eql("error")
                    res.body.message.should.be.eql("Aucun objet affecté")
                    done();
                })
        })
        it("Cela devrait supprimer un emplacement avec une id valide et un emplacement existant", (done) => {
            let id = 1;

            chai.request(server)
                .delete('/emplacements/' + id)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message');
                    res.body.message.should.be.eql("Suppression réussie")
                    done();
                })
        })
    });


});