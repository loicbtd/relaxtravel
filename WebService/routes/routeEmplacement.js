const DAOEmplacement = require('../data/DAOEmplacement');

let Joi = require('@hapi/joi');
const schemaEmplacement = Joi.object({
    id_emplacement: Joi.number().integer().min(1),
    etiquette: Joi.string().pattern(/^[a-zA-Z0-9]/).min(1).required()
}).required();

async function postEmplacement(req, res) {
    data = req.body;
    const { error, value } = schemaEmplacement.validate(data);
    if (!error) {
        let donnees = await DAOEmplacement.ajouter(value);
        if (donnees[0]) {
            res.json({
                status: 'success',
                id: donnees[1]
            });
        } else {
            res.json({
                status: 'error',
                message: donnees[1],
            });
        }
    } else {
        res.status(422).json({
            status: 'error',
            message: error.details[0].message
        });
    }
}

async function getEmplacements(req, res) {
    let donnees = await DAOEmplacement.lister();
    if (donnees[0]) {
        res.json({
            status: 'success',
            data: donnees[1]
        });
    } else {
        res.json({
            status: 'error',
            message: donnees[1]
        });
    }
}

async function getEmplacement(req, res) {
    const { id } = req.params;
    if (id <= 0 || isNaN(id)) {
        res.status(422).json({
            status: 'error',
            message: 'Erreur dans la requête'
        });
    } else {
        let donnees = await DAOEmplacement.recupererParId(id);
        if (donnees[0]) {
            res.json({
                status: 'success',
                data: donnees[1]
            });
        } else {
            res.status(422).json({
                status: 'error',
                message: 'Objet inexistant'
            });
        }
    }
}

async function updateEmplacement(req, res) {
    const { id } = req.params;
    data = req.body;
    if (id <= 0) {
        res.status(422).json({
            status: 'error',
            message: 'Erreur dans la requête'
        });
    } else {
        data.id_emplacement = id;
        const { error, value } = schemaEmplacement.validate(data);
        if (!error) {
            let donnees = await DAOEmplacement.modifier(value)
            if (donnees[0]) {
                res.json({
                    status: 'success',
                    message: donnees[1],
                });
            } else {
                res.status(422).json({
                    status: 'error',
                    message: donnees[1],
                });
            }
        } else {
            res.status(422).json({
                status: 'error',
                message: error.details[0].message
            });
        }

    }
}

async function deleteEmplacement(req, res) {
    const { id } = req.params;
    data = req.body;

    if (id <= 0 || isNaN(id)) {
        res.status(422).json({
            status: 'error',
            message: 'Erreur dans la requête',
        });
    } else {
        let donnees = await DAOEmplacement.supprimerParId(id);
        if (donnees[0]) {
            res.json({
                status: 'success',
                message: donnees[1]
            });
        } else {
            res.status(422).json({
                status: 'error',
                message: donnees[1]
            });
        }
    }
}

module.exports = {
    getEmplacements,
    postEmplacement,
    getEmplacement,
    deleteEmplacement,
    updateEmplacement
};