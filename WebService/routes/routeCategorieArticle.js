const DAOCategorieArticle = require('../data/DAOCategorieArticle');

let Joi = require('@hapi/joi');
const schemaCategorieArticle = Joi.object({
    id_categorie_article: Joi.number().integer().min(1),
    etiquette: Joi.string().pattern(/^[a-zA-Z]/).min(1).required()
}).required();

async function postCategorieArticle(req, res) {
    data = req.body;
    const { error, value } = schemaCategorieArticle.validate(data);
    if (!error) {
        let donnees = await DAOCategorieArticle.ajouter(value);
        if (donnees[0]) {
            res.json({
                status: 'success',
                id: donnees[1]
            });
        } else {
            res.json({
                status: 'error',
                message: donnees[1]
            });
        }
    } else {
        res.status(422).json({
            status: 'error',
            message: error.details[0].message
        });
    }
}

async function getCategoriesArticle(req, res) {
    let donnees = await DAOCategorieArticle.lister();
    if (donnees[0]) {
        res.json({
            status: 'success',
            data: donnees[1]
        });
    } else {
        res.json({
            status: 'error',
            message: donnees[1]
        });
    }
}

async function getCategorieArticle(req, res) {
    const { id } = req.params;
    if (id <= 0 || isNaN(id)) {
        res.status(422).json({
            status: 'error',
            message: 'Erreur dans la requête'
        });
    } else {
        let donnees = await DAOCategorieArticle.recupererParId(id);
        if (donnees[0]) {
            res.json({
                status: 'success',
                data: donnees[1]
            });
        } else {
            res.status(422).json({
                status: 'error',
                message: 'Objet inexistant'
            });
        }
    }
}

async function updateCategorieArticle(req, res) {
    const { id } = req.params;
    data = req.body;
    if (id <= 0) {
        res.status(422).json({
            status: 'error',
            message: 'Erreur dans la requête'
        });
    } else {
        data.id_categorie_article = id;
        const { error, value } = schemaCategorieArticle.validate(data);
        if (!error) {
            let donnees = await DAOCategorieArticle.modifier(value);
            if (donnees[0]) {
                res.json({
                    status: 'success',
                    message: donnees[1]
                });
            } else {
                res.status(422).json({
                    status: 'error',
                    message: donnees[1]
                });
            }
        } else {
            res.status(422).json({
                status: 'error',
                data: error.details[0].message
            });
        }
    }
}

async function deleteCategorieArticle(req, res) {
    const { id } = req.params;
    data = req.body;

    if (id <= 0 || isNaN(id)) {
        res.status(422).json({
            status: 'error',
            message: 'Erreur dans la requête'
        });
    } else {
        let donnees = await DAOCategorieArticle.supprimerParId(id);
        if (donnees[0]) {
            res.json({
                status: 'success',
                message: donnees[1]
            });
        } else {
            res.status(422).json({
                status: 'error',
                message: donnees[1]
            });
        }
    }
}

module.exports = {
    postCategorieArticle,
    getCategoriesArticle,
    getCategorieArticle,
    updateCategorieArticle,
    deleteCategorieArticle
};