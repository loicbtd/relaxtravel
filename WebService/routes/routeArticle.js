const DAOArticle = require('../data/DAOArticle');

let Joi = require('@hapi/joi');
const schemaArticle = Joi.object({
    id_article: Joi.number().integer().min(1),
    code: Joi.string().pattern(/^[0-9]/).min(13).max(13).required(),
    id_categorie_article: Joi.number().integer().min(1).required(),
    etiquette: Joi.string().pattern(/^[a-zA-Z]/).min(1).required(),
    caracteristiques: Joi.string().min(1).required()
}).required();

const shemaCode = Joi.object({
    code: Joi.string().pattern(/^[0-9]/).min(13).max(13).required(),
}).required();

async function postArticle(req, res) {
    data = req.body;
    const { error, value } = schemaArticle.validate(data);
    if (!error) {
        let donnees = await DAOArticle.ajouter(value);
        if (donnees[0]) {
            res.json({
                status: 'success',
                id: donnees[1]
            });
        } else {
            res.json({
                status: 'error',
                message: donnees[1]
            });
        }
    } else {
        res.status(422).json({
            status: 'error',
            message: error.details[0].message
        });
    }
}

async function getArticles(req, res) {
    let donnees = await DAOArticle.lister();
    if (donnees[0]) {
        res.json({
            status: 'success',
            data: donnees[1]
        });
    } else {
        res.json({
            status: 'error',
            message: donnees[1]
        });
    }
}

async function getArticle(req, res) {
    const { id } = req.params;
    if (id <= 0 || isNaN(id)) {
        res.status(422).json({
            status: 'error',
            message: 'Erreur dans la requête'
        });
    } else {
        let donnees = await DAOArticle.recupererParId(id);
        if (donnees[0]) {
            res.json({
                status: 'success',
                data: donnees[1]
            });
        } else {
            res.status(422).json({
                status: 'error',
                message: 'Objet inexistant'
            });
        }
    }
}

async function getArticleParCode(req, res) {
    const { code } = req.params;
    const { error, value } = shemaCode.validate({ code });
    if (!error) {
        let donnees = await DAOArticle.recupererParCodeBarre(value);
        if (donnees[0]) {
            res.json({
                status: 'success',
                message: donnees[1]
            });
        } else {
            res.status(422).json({
                status: 'error',
                message: donnees[1]
            });
        }
    } else {
        res.status(422).json({
            status: 'error',
            data: error.details[0].message
        });
    }
}

async function updateArticle(req, res) {
    const { id } = req.params;
    data = req.body;
    if (id <= 0) {
        res.status(422).json({
            status: 'error',
            message: 'Erreur dans la requête'
        });
    } else {
        data.id_article = id;
        const { error, value } = schemaArticle.validate(data);
        if (!error) {
            let donnees = await DAOArticle.modifier(value);
            if (donnees[0]) {
                res.json({
                    status: 'success',
                    message: donnees[1]
                });
            } else {
                res.status(422).json({
                    status: 'error',
                    message: donnees[1]
                });
            }
        } else {
            res.status(422).json({
                status: 'error',
                data: error.details[0].message
            });
        }
    }
}

async function deleteArticle(req, res) {
    const { id } = req.params;
    data = req.body;

    if (id <= 0 || isNaN(id)) {
        res.status(422).json({
            status: 'error',
            message: 'Erreur dans la requête'
        });
    } else {
        let donnees = await DAOArticle.supprimerParId(id);
        if (donnees[0]) {
            res.json({
                status: 'success',
                message: donnees[1]
            });
        } else {
            res.status(422).json({
                status: 'error',
                message: donnees[1]
            });
        }
    }
}

module.exports = {
    postArticle,
    getArticles,
    getArticle,
    getArticleParCode,
    deleteArticle,
    updateArticle
};