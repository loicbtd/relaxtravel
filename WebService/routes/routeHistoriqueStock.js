const DAOHistoriqueStock = require('../data/DAOHistoriqueStock');
const Joi = require('@hapi/joi');

const schemaHistoriqueStock = Joi.object({
    id_utilisateur: Joi.number().integer().min(1).required(),
    id_article: Joi.number().integer().min(1).required(),
    id_emplacement: Joi.number().integer().min(1).required(),
    quantite: Joi.number().integer().min(1).required(),
    id_type_transaction: Joi.number().integer().min(1).required()
}).required();

async function postHistoriqueStock(req, res) {
    data = req.body;
    const { error, value } = schemaHistoriqueStock.validate(data);
    if (!error) {
        let donnees = await DAOHistoriqueStock.ajouter(value);
        if (donnees[0]) {
            res.json({
                status: 'success',
                id: donnees[1]
            });
        } else {
            res.json({
                status: 'error',
                message: donnees[1]
            });
        }
    } else {
        res.status(422).json({
            status: 'error',
            message: error.details[0].message
        });
    }
}

async function getHistoriquesStock(req, res) {
    let donnees = await DAOHistoriqueStock.lister();
    if (donnees[0]) {
        res.json({
            status: 'success',
            data: donnees[1]
        });
    } else {
        res.json({
            status: 'error',
            message: donnees[1]
        });
    }
}

async function getHistoriqueStock(req, res) {
    const { id } = req.params;
    if (id <= 0 || isNaN(id)) {
        res.status(422).json({
            status: 'error',
            message: 'Erreur dans la requête'
        });
    } else {
        let donnees = await DAOHistoriqueStock.recupererParId(id);
        if (donnees[0]) {
            res.json({
                status: 'success',
                data: donnees[1]
            });
        } else {
            res.status(422).json({
                status: 'error',
                message: 'Objet inexistant'
            });
        }
    }
}

module.exports = { postHistoriqueStock, getHistoriquesStock, getHistoriqueStock };