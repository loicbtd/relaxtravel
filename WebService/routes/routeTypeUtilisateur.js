const DAOTypeUtilisateur = require('../data/DAOTypeUtilisateur');

let Joi = require('@hapi/joi');
const schemaTypeUtilisateur = Joi.object({
    id_type_utilisateur: Joi.number().integer().min(1),
    etiquette: Joi.string().pattern(/^[a-zA-Z0-9]/).min(1).required()
}).required();

async function postTypeUtilisateur(req, res) {
    data = req.body;
    const { error, value } = schemaTypeUtilisateur.validate(data);
    if (!error) {
        let donnees = await DAOTypeUtilisateur.ajouter(value);
        if (donnees[0]) {
            res.json({
                status: 'success',
                id: donnees[1]
            });
        } else {
            res.json({
                status: 'error',
                message: donnees[1]
            });
        }
    } else {
        res.status(422).json({
            status: 'error',
            message: error.details[0].message
        });
    }
}

async function getTypesUtilisateur(req, res) {
    let donnees = await DAOTypeUtilisateur.lister();
    if (donnees[0]) {
        res.json({
            status: 'success',
            data: donnees[1]
        });
    } else {
        res.json({
            status: 'error',
            message: donnees[1]
        });
    }
}

async function getTypeUtilisateur(req, res) {
    const { id } = req.params;
    if (id <= 0 || isNaN(id)) {
        res.status(422).json({
            status: 'error',
            message: 'Erreur dans la requête'
        });
    } else {
        let donnees = await DAOTypeUtilisateur.recupererParId(id);
        if (donnees[0]) {
            res.json({
                status: 'success',
                data: donnees[1]
            });
        } else {
            res.status(422).json({
                status: 'error',
                message: 'Objet inexistant'
            });
        }
    }
}

async function updateTypeUtilisateur(req, res) {
    const { id } = req.params;
    data = req.body;
    if (id <= 0) {
        res.status(422).json({
            status: 'error',
            message: 'Erreur dans la requête'
        });
    } else {
        data.id_type_utilisateur = id;
        const { error, value } = schemaTypeUtilisateur.validate(data);
        if (!error) {
            let donnees = await DAOTypeUtilisateur.modifier(value);
            if (donnees[0]) {
                res.json({
                    status: 'success',
                    message: donnees[1]
                });
            } else {
                res.status(422).json({
                    status: 'error',
                    message: donnees[1]
                });
            }
        } else {
            res.status(422).json({
                status: 'error',
                data: error.details[0].message
            });
        }
    }
}

async function deleteTypeUtilisateur(req, res) {
    const { id } = req.params;
    data = req.body;

    if (id <= 0 || isNaN(id)) {
        res.status(422).json({
            status: 'error',
            message: 'Erreur dans la requête'
        });
    } else {
        let donnees = await DAOTypeUtilisateur.supprimerParId(id);
        if (donnees[0]) {
            res.json({
                status: 'success',
                message: donnees[1]
            });
        } else {
            res.status(422).json({
                status: 'error',
                message: donnees[1]
            });
        }
    }
}

module.exports = {
    postTypeUtilisateur,
    getTypesUtilisateur,
    getTypeUtilisateur,
    updateTypeUtilisateur,
    deleteTypeUtilisateur
};