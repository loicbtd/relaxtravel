const DAOUtilisateur = require('../data/DAOUtilisateur');

let Joi = require('@hapi/joi');
const schemaUtilisateur = Joi.object({
    id_utilisateur: Joi.number().integer().min(1),
    code: Joi.string().pattern(/^[0-9]/).min(13).max(13).required(),
    id_type_utilisateur: Joi.number().integer().min(1).required(),
    identifiant: Joi.string().pattern(/^[a-zA-Z]/).min(1).required(),
    mot_de_passe: Joi.string().min(1).required()
}).required();

async function postUtilisateur(req, res) {
    data = req.body;
    const { error, value } = schemaUtilisateur.validate(data);
    if (!error) {
        let donnees = await DAOUtilisateur.ajouter(value);
        if (donnees[0]) {
            res.json({
                status: 'success',
                id: donnees[1]
            });
        } else {
            res.json({
                status: 'error',
                message: donnees[1]
            });
        }
    } else {
        res.status(422).json({
            status: 'error',
            message: error.details[0].message
        });
    }
}

async function getUtilisateurs(req, res) {
    let donnees = await DAOUtilisateur.lister();
    if (donnees[0]) {
        res.json({
            status: 'success',
            data: donnees[1]
        });
    } else {
        res.json({
            status: 'error',
            message: donnees[1]
        });
    }
}

async function getUtilisateur(req, res) {
    const { id } = req.params;
    if (id <= 0 || isNaN(id)) {
        res.status(422).json({
            status: 'error',
            message: 'Erreur dans la requête'
        });
    } else {
        let donnees = await DAOUtilisateur.recupererParId(id);
        if (donnees[0]) {
            res.json({
                status: 'success',
                data: donnees[1]
            });
        } else {
            res.status(422).json({
                status: 'error',
                message: 'Objet inexistant'
            });
        }
    }
}

async function updateUtilisateur(req, res) {
    const { id } = req.params;
    data = req.body;
    if (id <= 0) {
        res.status(422).json({
            status: 'error',
            message: 'Erreur dans la requête'
        });
    } else {
        data.id_utilisateur = id;
        const { error, value } = schemaUtilisateur.validate(data);
        if (!error) {
            let donnees = await DAOUtilisateur.modifier(value);
            if (donnees[0]) {
                res.json({
                    status: 'success',
                    message: donnees[1]
                });
            } else {
                res.status(422).json({
                    status: 'error',
                    message: donnees[1]
                });
            }
        } else {
            res.status(422).json({
                status: 'error',
                data: error.details[0].message
            });
        }
    }
}

async function deleteUtilisateur(req, res) {
    const { id } = req.params;
    data = req.body;

    if (id <= 0 || isNaN(id)) {
        res.status(422).json({
            status: 'error',
            message: 'Erreur dans la requête'
        });
    } else {
        let donnees = await DAOUtilisateur.supprimerParId(id);
        if (donnees[0]) {
            res.json({
                status: 'success',
                message: donnees[1]
            });
        } else {
            res.status(422).json({
                status: 'error',
                message: donnees[1]
            });
        }
    }
}

module.exports = {
    postUtilisateur,
    getUtilisateurs,
    getUtilisateur,
    updateUtilisateur,
    deleteUtilisateur
};