const DAOTypeTransaction = require('../data/DAOTypeTransaction');

let Joi = require('@hapi/joi');
const schemaTypeTransaction = Joi.object({
    id_type_transaction: Joi.number().integer().min(1),
    etiquette: Joi.string().pattern(/^[a-zA-Z0-9]/).min(1).required()
}).required();

async function postTypeTransaction(req, res) {
    data = req.body;
    const { error, value } = schemaTypeTransaction.validate(data);
    if (!error) {
        let donnees = await DAOTypeTransaction.ajouter(value);
        if (donnees[0]) {
            res.json({
                status: 'success',
                id: donnees[1]
            });
        } else {
            res.json({
                status: 'error',
                message: donnees[1]
            });
        }
    } else {
        res.status(422).json({
            status: 'error',
            message: error.details[0].message
        });
    }
}

async function getTypesTransaction(_req, res) {
    let donnees = await DAOTypeTransaction.lister();
    if (donnees[0]) {
        res.json({
            status: 'success',
            data: donnees[1]
        });
    } else {
        res.json({
            status: 'error',
            message: donnees[1]
        });
    }
}

async function getTypeTransaction(req, res) {
    const { id } = req.params;
    if (id <= 0 || isNaN(id)) {
        res.status(422).json({
            status: 'error',
            message: 'Erreur dans la requête'
        });
    } else {
        let donnees = await DAOTypeTransaction.recupererParId(id);
        if (donnees[0]) {
            res.json({
                status: 'success',
                data: donnees[1]
            });
        } else {
            res.status(422).json({
                status: 'error',
                message: 'Objet inexistant'
            });
        }
    }
}

async function updateTypeTransaction(req, res) {
    const { id } = req.params;
    data = req.body;
    if (id <= 0) {
        res.status(422).json({
            status: 'error',
            message: 'Erreur dans la requête'
        });
    } else {
        data.id_type_transaction = id;
        const { error, value } = schemaTypeTransaction.validate(data);
        if (!error) {
            let donnees = await DAOTypeTransaction.modifier(value);
            if (donnees[0]) {
                res.json({
                    status: 'success',
                    message: donnees[1]
                });
            } else {
                res.status(422).json({
                    status: 'error',
                    message: donnees[1]
                });
            }
        } else {
            res.status(422).json({
                status: 'error',
                data: error.details[0].message
            });
        }
    }
}

async function deleteTypeTransaction(req, res) {
    const { id } = req.params;
    data = req.body;

    if (id <= 0 || isNaN(id)) {
        res.status(422).json({
            status: 'error',
            message: 'Erreur dans la requête'
        });
    } else {
        let donnees = await DAOTypeTransaction.supprimerParId(id);
        if (donnees[0]) {
            res.json({
                status: 'success',
                message: donnees[1]
            });
        } else {
            res.status(422).json({
                status: 'error',
                message: donnees[1]
            });
        }
    }
}

module.exports = {
    getTypesTransaction,
    postTypeTransaction,
    getTypeTransaction,
    deleteTypeTransaction,
    updateTypeTransaction
};