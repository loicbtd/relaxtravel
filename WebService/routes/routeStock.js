const DAOStock = require('../data/DAOStock');


let Joi = require('@hapi/joi');
const shemaStock = Joi.object({
    id_article: Joi.number().integer().min(1),
    id_categorie_article: Joi.number().integer().min(1),
    id_emplacement: Joi.number().integer().min(1),
    id_type_transaction: Joi.number().integer().min(1),
    id_type_utilisateur: Joi.number().integer().min(1),
    id_utilisateur: Joi.number().integer().min(1)
}).required();

async function getStock(req, res) {
    data = req.query;
    const { error, value } = shemaStock.validate(data);
    if (!error) {
        let donnees = await DAOStock.lister(value);
        if (donnees[0]) {
            res.json({
                status: 'success',
                data: donnees[1]
            });
        } else {
            res.json({
                status: 'error',
                message: donnees[1]
            });
        }
    } else {
        res.status(422).json({
            status: 'error',
            message: error.details[0].message
        });
    }
}

module.exports = { getStock }