# RelaxTravel

par Adrian-Paul Carrières, Loïc Bertrand, Estevan Gay, Louis

## Table des matières

- [RelaxTravel](#relaxtravel)
  - [Table des matières](#table-des-mati%c3%a8res)
  - [Installation](#installation)
    - [Général](#g%c3%a9n%c3%a9ral)
      - [relaxtravel-app](#relaxtravel-app)
      - [WebService](#webservice)
  - [Fonctionnement de l'API](#fonctionnement-de-lapi)
    - [Explication du code](#explication-du-code)
    - [Routes](#routes)

## Installation

### Général

Les deux applications fonctionnant sous Node, nous allons l'installer en premier.

```bash
sudo apt-get install nodejs
```

Nous allons aussi récupérer le projet si nous ne l'avions pas déjà fait.

```bash
git clone https://gitlab.com/loicbtd/relaxtravel.git
```

#### relaxtravel-app

Il s'agit de notre application, du front-end, pour simplifier. Cordova nous permet de développer un seul code mais de déployer sous plusieurs formes : navigateur, application mobile, application electron... Il existe d'autres technologies pour arriver au même résultat.

```bash
# On installe cordova de manière globale
npm install -g cordova

# On entre dans le dossier de l'application pour installer les packets npm
cd relaxtravel-app
npm install

# On installe les plateformes, ici uniquement le browser
cordova platform add browser
cordova build browser
cordova run browser
```

#### WebService

Notre WebService est une simple API faite via Express.js, qui se connecte à notre base de données PostGreSQL. Pourquoi Express ? Parce que nous connaissions déjà cette technologie, facile à prendre en main, et que nous souhaitions développer nos connaissances avec.

Pour pouvoir se servir de notre API, il faudra commencer par paramétrer le fichier /WebService/data/BaseDeDonnees.js avec vos credentials.

```bash
# A priori depuis la racine du projet
cd WebService

# On a ici deux choix : installer les dépendances de dev...
npm install

#... ou en production, donc sans !
npm install --production

# On peut maintenant lancer l'application d'un simple
npm start

# Si nous avons installé toutes les dépendances il est aussi possible de lancer les tests
npm test

# Pour la couverture
nyc --check-coverage npm run test
```

Il existe une dernière possibilité, c'est celle de Docker (dont vous aurez donc besoin).

```bash
# Depuis la racine du projet

# Nous construisons l'image en premier
docker build -t relaxtravel WebService

# Puis, au cas où le conteneur tournerait déjà, nous le coupons et le détruisons
(docker stop relaxtravel && dokcer rm relaxtravel) || echo "Aucun conteneur RelaxTravel détecté"

# Nous pouvons maintenant en démarrer un nouveau, ici au port 8080.
docker run --name relaxtravel -d -p 8080:8080
```

## Fonctionnement de l'API

### Explication du code

[![Hi](https://mermaid.ink/img/eyJjb2RlIjoiZ3JhcGggVERcblx0QVtyZWxheHRyYXZlbC1hcHBdIC0tPnxHRVQgL2VtcGxhY2VtZW50c3wgQihzZXJ2ZXIuanMpXG5cdEEgLS0-fEdFVCAvZW1wbGFjZW1lbnRzLzF8IEJcblx0QSAtLT58UE9TVCAvZW1wbGFjZW1lbnRzfCBCXG5cdEEgLS0-fFBVVCAvZW1wbGFjZW1lbnRzLzF8IEJcblx0QSAtLT58REVMRVRFIC9lbXBsYWNlbWVudHMvMXwgQlxuXHRCIC0tPnxnZXRFbXBsYWNlbWVudHN8IEMocm91dGVFbXBsYWNlbWVudC5qcylcblx0QiAtLT58Z2V0RW1wbGFjZW1lbnR8IENcblx0QiAtLT58cG9zdEVtcGxhY2VtZW50fCBDXG5cdEIgLS0-fHVwZGF0ZUVtcGxhY2VtZW50fCBDXG5cdEIgLS0-fGRlbGV0ZUVtcGxhY2VtZW50fCBDXG5cdEMgLS0-fGxpc3RlcnwgRChEQU9FbXBsYWNlbWVudC5qcylcblx0QyAtLT58cmVjdXBlcmVyUGFySWQgMXwgRFxuXHREIC0tPnxTUUxfTElTVEVSfCBFKFNRTEVtcGxhY2VtZW50KVxuXHREIC0tPiB8U1FMX1RST1VWRVJfUEFSX0lEfCBFXG5cdEUgLS0-IEYoQmFzZSBkZSBkb25uw6llcylcbiIsIm1lcm1haWQiOnsidGhlbWUiOiJmb3Jlc3QifX0)](https://mermaid-js.github.io/mermaid-live-editor/#/edit/eyJjb2RlIjoiZ3JhcGggVERcblx0QVtyZWxheHRyYXZlbC1hcHBdIC0tPnxHRVQgL2VtcGxhY2VtZW50c3wgQihzZXJ2ZXIuanMpXG5cdEEgLS0-fEdFVCAvZW1wbGFjZW1lbnRzLzF8IEJcblx0QSAtLT58UE9TVCAvZW1wbGFjZW1lbnRzfCBCXG5cdEEgLS0--fHVwZGF0ZUVtcGxhY2VtZW50fCBDXG5cdEIgLS0-fGRlbGV0ZUVtcGxhY2VtZW50fCBDXG5cdEMgLS0-fGxpc3RlcnwgRChEQU9FbXBsYWNlbWVudC5qcylcblx0QyAtLT58cmVjdXBlcmVyUGFySWQgMXwgRFxuXHREIC0tPnxTUUxfTElTVEVSfCBFKFNRTEVtcGxhY2VtZW50KVxuXHREIC0tPiB8U1FMX1RST1VWRVJfUEFSX0lEfCBFXG5cdEUgLS0-IEYoQmFzZSBkZSBkb25uw6llcylcbiIsIm1lcm1haWQiOnsidGhlbWUiOiJmb3Jlc3QifX0) [Schéma simplifié du fonctionnement de l'API]

Express permet de gérer la réécriture d'URL et la différenciation des verbes HTTP sur une même URL très facilement. L'utilisation de middle ware est aussi très facile à mettre en place.

Dans ce simple exemple de code, on redirige les requêtes GET, POST, DELETE et PUT sur une URL à un gestionnaire de route dédié uniquement à une ressource, ici, les emplacements. Cela permet de garder, dans server.js, un code propre et assez minimaliste, car c'est aussi dans ce même fichier qu'on ajoute les dépendances chargées de gérer le CORS, le parsage en JSON...

```js
app.route("/emplacements")
    .get(routeEmplacement.getEmplacements)
    .post(routeEmplacement.postEmplacement)
app.route("/emplacements/:id")
    .get(routeEmplacement.getEmplacement)
    .delete(routeEmplacement.deleteEmplacement)
    .put(routeEmplacement.updateEmplacement)
```

Chaque fonction appelée dans le snippet de code précédent possède elle aussi un seul but. Elle va d'abord se charger d'utiliser le schéma pour vérifier la requête, puis elle actionnera la fonction correspondante du [DAO (Data Access Object)](https://fr.wikipedia.org/wiki/Objet_d%27acc%C3%A8s_aux_donn%C3%A9es) de la même ressource.

```js
const schemaEmplacement = Joi.object({
    id_emplacement: Joi.number().integer().min(1),
    etiquette: Joi.string().pattern(/^[a-zA-Z0-9]/).min(1).required()
}).required();

async function updateEmplacement(req, res) {
    const { id } = req.params;
    data = req.body;
    if (id <= 0) {
        res.status(422).json({
            status: 'error',
            message: 'Erreur dans la requête'
        });
    }
    else {
        data.id_emplacement = id;
        const { error, value } = schemaEmplacement.validate(data);
        if (!error) {
            let donnees = await DAOEmplacement.modifier(value)
            if (donnees[0]) {
                res.json({
                    status: 'success',
                    message: donnees[1],
                });
            }
            else {
                res.status(422).json({
                    status: 'error',
                    message: donnees[1],
                });
            }
        }
        else {
            res.status(422).json({
                status: 'error',
                message: error.details[0].message
            });
        }

    }
}
```

C'est dans le DAO qu'on éxécutera des query à la base de données, préparées ici dans le fichier SQLEmplacement. C'est ce qu'on appelle des [prepared statements](https://en.wikipedia.org/wiki/Prepared_statement).

```js
modifier = async function(emplacement) {
    let baseDeDonnees = BaseDeDonnees.getInstance();
    let parametres_requete = [emplacement.id_emplacement, emplacement.etiquette];
    try {
        await baseDeDonnees.query('BEGIN');
        let resultat = await baseDeDonnees.query(SQLEmplacement.SQL_MODIFIER, parametres_requete);
        await baseDeDonnees.query('COMMIT');
        if (resultat.rowCount > 0){
            return [true, "Modification réussie"];
        }
        else {
            return [false, "Aucun object affecté"];
        }
    } catch(erreur){
        await baseDeDonnees.query('ROLLBACK');
        return [false, erreur.stack];
    }
}
```

```js
SQL_MODIFIER = "UPDATE emplacement SET etiquette=$2 WHERE id_emplacement=$1";
```

En regardant de plus près, nous pouvons voir que notre approche (relativement) fonctionnelle nous permet de retourner, à chaque étape, les erreurs au client, ou, à la toute fin le résultat de sa requête très facilement. Sur le schéma précédent, nous pouvons en effet imaginer que chaque flèche est en fait une flèche à double sens. Cela pourrait être achever plus facilement avec d'autres bibliothèques, middleware, framework ou même d'autres langages de programmation plus adaptés.

Notre code est ainsi très modulable, et il est facile d'ajouter de nouvelles requêtes, de nouvelles routes et ressources, en ajoutant en général une fonction par requête à chaque couche de notre architecture. Cela en fait une application back-end maintenable et capable d'évoluer.

### Routes

Notre API est basée sur un type de contenu JSON. La plupart des requêtes simples, c'est à dire, un CRUD sur notre base de données sont à faire sur le modèle d'URL suivant, avec id l'identifiant d'une ressource en particulier.

* /ressources (verbes GET, POST)
* /ressources/id (verbes GET, PUT, DELETE)

Ainsi, nous avons :

* /emplacements
* /emplacements/id
* /categories-article
* /categories-article/id
* /articles
* /articles/id
* /articles-code/code (pour récupérer un article via son code)
* /types-transaction
* /types-transaction/id
* /types-utilisateur
* /types-utilisateur/id
* /types-utilisateur/id/utilisateurs (pour récupérer tous les utilisateurs d'un type utilisateur)
* /utilisateurs
* /utilisateurs/id
* /historiques-stock
* /historiques-stock/id

Il reste une dernière route un peu particulière, celle du stock :
* /stock

En effet, celle-ci s'accompagne de paramètres ci-dessous, ce qui retourne à l'utilisateur une liste filtrée (si paramètre il y a), des produits disponibles :
* id_article
* id_categorie_article
* id_emplacement
* id_type_transaction
* id_type_utilisateur
* id_utilisateur

Pour pouvoir utiliser ces filtres, il faut écrire l'URL sous cette forme :
http://relaxtravel.adrianpaul-carrieres.fr/stock?id_categorie_article=1&id_utilisateur=1