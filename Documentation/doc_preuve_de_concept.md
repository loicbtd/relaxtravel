# Preuve de Concept

## Applications

Notre projet serait organisé en une application client et une application serveur, qui permettent de régir différents axes :

* L'application client permettra de gérer les entrepôts et les différents produits (?) côté administration, côté utilisateur elle permettra de faire entrer et sortir des produits d'un entrepôt en scannant un code QR ou un code barre.
* Le webservice fera office d'interface entrée/sortie vers la ou les base(s) de données.

Pour l'application client nous utiliserons du HTML/CSS/JavaScript et Apache Cordova ; côté serveur nous utiliserons NodeJS.

## Bases de données

Nous avons besoin de stocker les informations propres aux utilisateurs, aux entrepôts et aux produits, ainsi qu'enregistrer les entrées et sorties des produits (que nous appelerons "transactions").
Nous souhaiterions utiliser une base de données SQL classique. Il est aussi possible d'utiliser un serveur de cache pour réduire la charge de la base de données en mettant en cache des informations souvent demandées.

Nous utiliserons donc PostGreSQL et possiblement Redis.

## Fonctionnement

L'application demandera aux utilisateurs de se connecter. Cela leur permettra de gérer les entrepôts s'ils ont des accès administrateurs, autrement ils ne pourront que déplacer des produits dans ou hors d'un entrepôt.

Il faut donc en plus ajouter un système d'inscription et de gestion d'un utilisateur, avec, on peut imaginer, la possibilité d'inscrire un administrateur qu'à partir d'un autre administrateur.

Nous pensons que ce système de compte utilisateur serait baser sur l'adresse mail et un mot de passe.

L'administrateur pourra ajouter, modifier, supprimer des entrepôts, peut-être les catégories et les produits qui existent, tandis qu'un utilisateur simple pourra, en scannant un code barre, identifier un produit et le faire entrer ou sortir de l'entrepôt.

Il faudra aussi pouvoir calculer des statistiques, travail qui sera effectué par le webservice.

## Difficultés à prévoir

La partie la plus longue sera probablement de réaliser les interfaces et la lecture du code barre, côté serveur il faudra aussi réaliser un système d'authentification.

Pour le reste nous possédons déjà une bonne partie des connaissances nécessaires à la réalisation du projet.
