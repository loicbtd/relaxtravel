L'infrastructure de notre projet se décompose en 4 parties:

* Une application administrateur côté client
* Une application employé côté client
* Une API REST (=Representational State Transfer) côté serveur
* Une base de données PostreSQL côté serveur

# Base de données

Nous avons choisi de mettre en place la base données dans un conteneur Docker afin de garantir la possibilité d'un déploiement sur n'importe quel poste de travail ou serveur indépendamment du système d'exploitation sous lequel il fonctionne. De plus, d'un point de vue sécurité, cela permet d'isoler la base de données du système du reste du système hôte.

## Installation de la base de données

### Prérequis

Il faut s'assurer d'avoir les paquets docker et docker-compose installés et démarrés. Ci-dessous, vous trouverez comment installer Docker sous Ubuntu. Vous pouvez vous référer à la page suivante pour les autres systèmes d'exploitation: https://docs.docker.com


* Installation des paquets

```shell
sudo apt-get update

sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common && 

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - 

sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io
```

* Démarrage et activation de Docker

```shell
sudo systemctl start docker
sudo systemctl enable docker
```

* Comment tester si Docker est correctement installé :

```shell
sudo docker run hello-world
```

> Si le message renvoyé par cette commande commence par "Hello from Docker !", tout est maintenant installé et vous pouvez passer à la suite. Sinon, référez-vous à la documentation dont le lien a été mentionné précédemment.


### Installation

1. Vous devez créer l'arborescence suivante :

├── dockerapp
│  └── postgres
│     └── docker-compose.yml

Pour cela, placez-vous dans le répertoire de votre choix et executer les commandes ci-dessous :

```shell
mkdir -p ./dockerapp/postgres
touch ./dockerapp/postgres/docker-compose.yml
```

3. Placez-vous dans le répertoire de postgres :

```shell
cd ./dockerapp/postgres
```

4. Éditez le fichier docker-compose.yml et copiez-collez la configuration suivante:

```yaml
version: '3'
services:
  postgres:
    image: postgres:10.10
    restart: always
    container_name: postgres
    command: postgres -c port=2232 -c shared_buffers=80MB -c max_connections=300
    environment:
      - PGDATA=data
      - POSTGRES_USER=admin
      - POSTGRES_PASSWORD=password
      - POSTGRES_DB=postgres
    volumes:
      - ./data:/data
    ports:
      - 2232:2232
```

NB: libre à vous de changer les valeurs de configuration par défaut ci-dessus afin de sécuriser ou non votre future instance de PostgreSQL.

5. Lancez le conteneur avec la commande suivante :

```shell
sudo docker-compose up -d
```

NB: L'option -d (=detach) vous permet de lancer le conteneur en mode "détaché" afin de ne pas devoir laisser le terminal ouvert.

6. Voir les logs de la base de données (optionnel)

```shell
sudo docker-compose logs -f
```

NB: L'option -f (=follow) vous permet de suivre les logs du conteneur. Pour arrêter de les suivre, faites Ctrl+C.


## Initialisation de la base de données

1. Téléchargez le repository du projet :

```shell
git clone https://gitlab.com/loicbtd/relaxtravel.git relaxtravel
```
2. Placez-vous dans le répertoire du projet et naviguez jusqu'au répertoire data :

```shell
cd relaxtravel
cd ./base-de-donnees/donnees
```

3. Assurez-vous que les deux scripts d'initialisation aient les droits d'écriture :

```shell
chmod +x ./setup-database
chmod +x ./start-postgresql-client
```

4. Éditez le fichier setup-database afin de remplacer éventuellement les valeurs par défaut par vos propres valeurs de configuration de base de données.

```shell
export PGPASSWORD=$MOT_DE_PASSE_ADMIN_BASE_DE_DONNEES
psql -U $ADMIN_BASE_DE_DONNEES -d $NOM_DE_LA_BASE_DE_DONNEES_ADMIN -h $HOTE_DE_LA_BASE_DE_DONNEES -p $PORT_DE_LA_BASE_DE_DONNEE -c "CREATE USER relaxtravel WITH PASSWORD '$MOT_DE_PASSE_DE_LA_BASE_DE_DONNEES_RELAXTRAVEL';"
psql -U $ADMIN_BASE_DE_DONNEES -d $NOM_DE_LA_BASE_DE_DONNEES_ADMIN -h $HOTE_DE_LA_BASE_DE_DONNEES -p $PORT_DE_LA_BASE_DE_DONNEE -c "CREATE DATABASE relaxtravel;"
psql -U $ADMIN_BASE_DE_DONNEES -d $NOM_DE_LA_BASE_DE_DONNEES_ADMIN -h $HOTE_DE_LA_BASE_DE_DONNEES -p $PORT_DE_LA_BASE_DE_DONNEE -c "GRANT ALL PRIVILEGES ON DATABASE relaxtravel to relaxtravel;"

```

5. Éditez le fichier start-postgresql-client afin de remplacer éventuellement les valeurs par défaut par vos propres valeurs de configuration de base de données.

```shell
#!/bin/sh
export PGPASSWORD="$MOT_DE_PASSE_DE_LA_BASE_DE_DONNEES_RELAXTRAVEL"
psql -U $UTILISATEUR_DE_LA_BASE_DE_DONNEES -d $NOM_DE_LA_BASE_DE_DONNEES -h $HOTE_DE_LA_BASE_DE_DONNEES -p $PORT_DE_LA_BASE_DE_DONNEES -f init_projet_tutore.sql
psql -U $UTILISATEUR_DE_LA_BASE_DE_DONNEES -d $NOM_DE_LA_BASE_DE_DONNEES -h $HOTE_DE_LA_BASE_DE_DONNEES -p $PORT_DE_LA_BASE_DE_DONNEES -f mock_projet_tutore.sql
```

6. Executez les deux scripts :

```shell
./setup-database
./start-postgresql-client
```

Voilà, la base de données est maintenant prête pour son utilisation !
