DROP TABLE IF EXISTS historique_stock;
DROP TABLE IF EXISTS type_transaction;
DROP TABLE IF EXISTS article;
DROP TABLE IF EXISTS utilisateur;
DROP TABLE IF EXISTS emplacement;
DROP TABLE IF EXISTS type_utilisateur;
DROP TABLE IF EXISTS categorie_article;

CREATE TABLE categorie_article(
    id_categorie_article serial PRIMARY KEY,
    etiquette text
);

CREATE TABLE type_utilisateur(
    id_type_utilisateur serial PRIMARY KEY,
    etiquette text
);

CREATE TABLE emplacement(
    id_emplacement serial PRIMARY KEY,
    etiquette text
);

CREATE TABLE utilisateur(
    id_utilisateur serial PRIMARY KEY,
    code text,
    id_type_utilisateur integer,
    identifiant text,
    mot_de_passe text,
    CONSTRAINT type_utilisateur_utilisateur_fk
        FOREIGN KEY (id_type_utilisateur)
        REFERENCES type_utilisateur(id_type_utilisateur)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE article(
    id_article serial PRIMARY KEY,
    code text,
    id_categorie_article integer,
    etiquette text,
    caracteristiques text,
    CONSTRAINT categorie_article_article_fk
        FOREIGN KEY (id_categorie_article)
        REFERENCES categorie_article(id_categorie_article)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

CREATE TABLE type_transaction(
    id_type_transaction serial PRIMARY KEY,
    etiquette text
);

CREATE TABLE historique_stock(
    id_historique_stock serial PRIMARY KEY,
    id_article integer,
    id_utilisateur integer,
    id_emplacement integer,
    date_saisie timestamp without time zone,
    quantite integer,
    id_type_transaction integer,
    CONSTRAINT article_historique_stock_fk
        FOREIGN KEY (id_article)
        REFERENCES article(id_article)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    CONSTRAINT utilisateur_historique_stock_fk
        FOREIGN KEY (id_utilisateur)
        REFERENCES utilisateur(id_utilisateur)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    CONSTRAINT emplacement_historique_stock_fk
        FOREIGN KEY (id_emplacement)
        REFERENCES emplacement(id_emplacement)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    CONSTRAINT type_transaction_historique_stock_fk
        FOREIGN KEY (id_type_transaction)
        REFERENCES type_transaction(id_type_transaction)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);