\COPY categorie_article (etiquette) FROM './mockdata/categorie_article.csv' DELIMITER ',' CSV;
\COPY type_utilisateur (etiquette) FROM './mockdata/type_utilisateur.csv' DELIMITER ',' CSV;
\COPY emplacement (etiquette) FROM './mockdata/emplacement.csv' DELIMITER ',' CSV;
\COPY utilisateur (code,id_type_utilisateur,identifiant,mot_de_passe) FROM './mockdata/utilisateur.csv' DELIMITER ',' CSV;
\COPY article (code,id_categorie_article,etiquette,caracteristiques) FROM './mockdata/article.csv' DELIMITER ',' CSV;
\COPY type_transaction (etiquette) FROM './mockdata/type_transaction.csv' DELIMITER ',' CSV;
\COPY historique_stock (id_article,id_utilisateur,id_emplacement,date_saisie,quantite, id_type_transaction) FROM './mockdata/historique_stock.csv' DELIMITER ',' CSV;
