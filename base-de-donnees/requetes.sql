-- table emplacement
-- create
INSERT INTO emplacement(id_emplacement, etiquette) VALUES(?,?);
-- read
SELECT id_emplacement, etiquette FROM emplacement;
SELECT id_emplacement, etiquette FROM emplacement WHERE  id_emplacement = ?;
-- update
UPDATE emplacement SET etiquette = ? WHERE id_emplacement = ?;
-- delete
DELETE FROM emplacement WHERE id_emplacement = ?;